package es.bsc.inb.etransafe.srdomain.rest.api.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import es.bsc.inb.etransafe.srdomain.rest.api.model.Document;

/**
 * Custom implementation of DocumentRepository
 * @author jcorvi
 *
 */
public class DocumentRepositoryImpl implements DocumentRepositoryCustom{

	@Autowired
    MongoTemplate mongoTemplate;
	
	public List<Document> findAllNamesByOrderByName(){
		Query query = new Query();
		query.fields().include("documentId");
		query.fields().include("fileName");
		query.fields().include("name");
		query.fields().include("processDate");
		query.fields().include("status");
		query.fields().include("recordState");
		query.addCriteria(Criteria.where("recordState").ne("DELETED"));
		//Sort sort = new Sort(Sort.Direction.ASC, "name");
		//query.with(sort);
		List<Document> documents = mongoTemplate.find(query,  Document.class);
		return documents;
	}

	public List<Document> findAllStudyIds(){
		Query query = new Query();
		query.fields().include("documentId");
		query.fields().include("studyId");
		query.addCriteria(Criteria.where("recordState").ne("DELETED"));
		//Sort sort = new Sort(Sort.Direction.ASC, "name");
		//query.with(sort);
		List<Document> documents = mongoTemplate.find(query,  Document.class);
		return documents;
	}
	
	
	
	
	
	
	

}
