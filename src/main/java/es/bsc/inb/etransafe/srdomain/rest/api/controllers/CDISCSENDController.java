package es.bsc.inb.etransafe.srdomain.rest.api.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCField;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodeList;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodedValue;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.FieldName;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Manifestation;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.SDomain;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Sex;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.TreatmentRelated;
import es.bsc.inb.etransafe.srdomain.rest.api.services.CDISCService;


@RestController
@CrossOrigin(origins="*")
@RequestMapping("/list")
public class CDISCSENDController {

	@Autowired
	private CDISCService cdiscService;
	
	@GetMapping(value="/manifestation_finding")
    public ResponseEntity<Map<Manifestation, String>> findAllManifestation() {
		Map<Manifestation, String> manifestationToLabel = Arrays.asList(Manifestation.values()).stream()
				.collect(Collectors.toMap(manifestationCode -> manifestationCode, manifestationCode -> manifestationCode.getDisplayName()));
		return new ResponseEntity<>(manifestationToLabel, HttpStatus.OK);
	}
	
	@GetMapping(value="/treatment_related")
    public ResponseEntity<Map<TreatmentRelated, String>> findAllTreatmentRelated() {
		Map<TreatmentRelated, String> treatmentRelatedToLabel = Arrays.asList(TreatmentRelated.values()).stream()
				.collect(Collectors.toMap(treatmentRelatedCode -> treatmentRelatedCode, treatmentRelatedCode -> treatmentRelatedCode.getDisplayName()));
		return new ResponseEntity<>(treatmentRelatedToLabel, HttpStatus.OK);
	}
	
	@GetMapping(value="/sex")
    public ResponseEntity<Map<Sex, String>> findAllSex() {
		Map<Sex, String> sexToLabel = Arrays.asList(Sex.values()).stream()
				.collect(Collectors.toMap(sexCode -> sexCode, sexCode -> sexCode.getDisplayName()));
		return new ResponseEntity<>(sexToLabel, HttpStatus.OK);
	}
	
	@GetMapping(value="/species")
    public ResponseEntity<CDISCField> findAllSpecies() {
		return new ResponseEntity<>(cdiscService.findByFieldName(FieldName.SRSPEC.name()), HttpStatus.OK);
	}
	
	@GetMapping(value="/study_domains")
    public ResponseEntity<CDISCField> findAllStudyDomains() {
		CDISCField field = cdiscService.findAllStudyDomains();
		if (field == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(field, HttpStatus.OK);
	}
	
	@GetMapping(value="/findings")
    public ResponseEntity<CDISCField> findAllFindings() {
		CDISCField field = cdiscService.findByFieldName(FieldName.SRFNDG.name());
		if (field == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(field, HttpStatus.OK); 
	}
	
	@GetMapping(value="/study_testcd")
    public ResponseEntity<CDISCField> findAllStudyTestCD() {
		CDISCField field = cdiscService.findByFieldName(FieldName.SRTSTCD.name());
		if (field == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(field, HttpStatus.OK); 
	}
	
	@GetMapping(value="/study_testcd_with_domain")
    public ResponseEntity<Map<SDomain, CodeList>> findAllStudyTestCdWithDomain() {
		Map<SDomain, CodeList> testCdByDomain = cdiscService.findAllTestCdsWithDomain();
		return new ResponseEntity<>(testCdByDomain, HttpStatus.OK); 
	}
	
	@GetMapping(value = "/study_testcd/{domain}")
	public ResponseEntity<CodeList> findAllStudyTestByDomain(@PathVariable(value="domain") SDomain domain) {
		CodeList codeList = cdiscService.findTestCDByDomain(domain);
		if (codeList == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(cdiscService.findTestCDByDomain(domain), HttpStatus.OK);
	}
	
	@GetMapping(value="/study_phases")
    public ResponseEntity<List<String>> findStudyPhases() {
		List<String> studyPhases = new ArrayList<>(Arrays.asList("PRE-DOSING", "DOSING", "RECOVERY"));
		return new ResponseEntity<>(studyPhases, HttpStatus.OK);
	}
	
	@GetMapping(value = "/species/{domain}")
	public ResponseEntity<List<CodedValue>> findAllSpeciesByDomain(@PathVariable(value="domain") SDomain domain) {
		List<CodedValue> codedValues = cdiscService.findSpeciesByDomain(domain);
		if (codedValues == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(codedValues, HttpStatus.OK);
	}
	
	@GetMapping(value = "/findings/{domain}")
	public ResponseEntity<List<CodedValue>> findAllFindingsByDomain(@PathVariable(value="domain") SDomain domain) {
		List<CodedValue> codedValues = cdiscService.findFindingsByDomain(domain);
		if (codedValues == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(codedValues, HttpStatus.OK);
	}
	
	//Another method that return study_testcd, but in this case returns directly CodedValues as a list.
	@GetMapping(value = "/study_testcd_cv/{domain}")
	public ResponseEntity<List<CodedValue>> findAllStudyTestByDomainCodedValues(@PathVariable(value="domain") SDomain domain) {
		List<CodedValue> codedvalues = cdiscService.findTestCdCodedValuesByDomain(domain);
		if (codedvalues == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(codedvalues, HttpStatus.OK);
	}
	
	//This method should be used because only specific domains should be retrieved.  
	//and it returns directly CodedValues as a list.
	@GetMapping(value="/study_domains_cv")
    public ResponseEntity<List<CodedValue>> findAllStudyDomainsCodedValues() {
		List<CodedValue> codedValues = cdiscService.findAllStudyDomainsCodedValues();
		if (codedValues == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(codedValues, HttpStatus.OK);
	}
	
	@GetMapping(value="/severity")
    public ResponseEntity<List<CodedValue>> findSeverityCodeValues() {
		List<CodedValue> codedValues = cdiscService.findSeverityCodedValues();
		if (codedValues == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(codedValues, HttpStatus.OK);
	}
	
}