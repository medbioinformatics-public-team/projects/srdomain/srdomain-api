package es.bsc.inb.etransafe.srdomain.rest.api.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCField;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodeList;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodedValue;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Constants;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.FieldName;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Finding;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.SDomain;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.SRDomain;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Specimen;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.TestShortName;
import es.bsc.inb.etransafe.srdomain.rest.api.repository.CDISCFieldRepository;

@Service
public class CDISCServiceImpl implements CDISCService {

	@Autowired
	private CDISCFieldRepository cdiscFieldRepository;

	@Override
	public List<CDISCField> findAll() {
		return cdiscFieldRepository.findAll();
	}

	@Override
	public CDISCField save(CDISCField document) {
		return cdiscFieldRepository.save(document);
	}

	@Override
	public CDISCField findByFieldName(String fieldName) {
		return cdiscFieldRepository.findByName(fieldName);
	}

	@Override
	public CodeList findTestCDByDomain(SDomain sdomain) {
		TestShortName testShortName = Constants.SDOMAIN_TESTSHORTNAME.get(sdomain);
		if (testShortName == null) {
			return null;
		}
		
		return cdiscFieldRepository.findByName(FieldName.SRTSTCD.name()).getCodeLists().stream()
				.filter(codeList -> Constants.SDOMAIN_TESTSHORTNAME.get(sdomain).name().equals(codeList.getName()))
				.findFirst()
				.orElse(null);
	}

	public List<CodedValue> findTestCdCodedValuesByDomain(SDomain sdomain) {
		List<SDomain> nonTestDomains = new ArrayList<>(Arrays.asList(SDomain.CL, SDomain.MI, SDomain.MA));
		if (nonTestDomains.contains(sdomain)) {
			return null;
		}

		return cdiscFieldRepository.findByName(FieldName.SRTSTCD.name()).getCodeLists().stream()
				.filter(codeList -> Constants.SDOMAIN_TESTSHORTNAME.get(sdomain).name().equals(codeList.getName()))
				.findFirst().get().getCodedValues();
	}
	
	public Map<SDomain, CodeList> findAllTestCdsWithDomain() {
		Map<SDomain, CodeList> testCdsByDomain = new HashMap<>();
		CDISCField field = cdiscFieldRepository.findByName(FieldName.SRTSTCD.name());
		Constants.SDOMAIN_TESTSHORTNAME.forEach((sdomain, testCd) -> {
			CodeList codeList = field.getCodeLists().stream()
					.filter(cl -> testCd.name().equals(cl.getName()))
					.findFirst()
					.orElse(null);
			
			if (codeList != null) {
				testCdsByDomain.put(sdomain, codeList);				
			}
		});
		
		return testCdsByDomain;
	}

	public List<CodedValue> findSpeciesByDomain(SDomain domain) {
		List<SDomain> specDomains = Arrays.asList(SDomain.MA, SDomain.MI, SDomain.LB, SDomain.OM, SDomain.PP);
		if (domain.equals(SDomain.CL)) {
			return cdiscFieldRepository.findByName(FieldName.SRSPEC.name()).getCodeLists().stream()
					.filter(codeList -> codeList.getName().equals(Specimen.BODSYS.name()))
					.findFirst()
					.get()
					.getCodedValues();
		} else if (specDomains.contains(domain)) {
			return cdiscFieldRepository.findByName(FieldName.SRSPEC.name()).getCodeLists().stream()
					.filter(codeList -> codeList.getName().equals(Specimen.SPEC.name()))
					.findFirst()
					.get()
					.getCodedValues();
		}
		
		return null;
	}

	public List<CodedValue> findFindingsByDomain(SDomain domain) {
		if (domain.equals(SDomain.CL)) {
			return cdiscFieldRepository.findByName(FieldName.SRFNDG.name()).getCodeLists().stream()
					.filter(codeList -> codeList.getName().equals(Finding.CLCAT.name()))
					.findFirst()
					.get()
					.getCodedValues();
		} else if (domain.equals(SDomain.MA) || domain.equals(SDomain.MI)) {
			return cdiscFieldRepository.findByName(FieldName.SRFNDG.name()).getCodeLists().stream()
					.filter(codeList -> (codeList.getName().equals(Finding.NEOPLASM.name())
							|| codeList.getName().equals(Finding.NONNEO.name())))
					.findFirst()
					.get()
					.getCodedValues();
		}
		
		return null;
	}

	public List<CodedValue> findAllStudyDomainsCodedValues() {
		return cdiscFieldRepository.findByName(FieldName.SRDOMAIN.name()).getCodeLists().stream()
				.filter(codeList -> codeList.getName().equals(SRDomain.SDOMAIN.name()))
				.findFirst()
				.get()
				.getCodedValues().stream()
				.filter(codeValue -> EnumUtils.isValidEnum(SDomain.class, codeValue.getName()))
				.collect(Collectors.toList());
	}

	public CDISCField findAllStudyDomains() {
		CDISCField field = this.findByFieldName(FieldName.SRDOMAIN.name());
		List<CodedValue> codedValues = field.getCodeLists().get(0).getCodedValues().stream()
			.filter(codedValue -> EnumUtils.isValidEnum(SDomain.class, codedValue.getName()))
			.collect(Collectors.toList());
		
		field.getCodeLists().get(0).setCodedValues(codedValues);
		return field;
	}

	public List<CodedValue> findSeverityCodedValues() {
		CodeList codeList = cdiscFieldRepository.findByName(FieldName.SRSEV.name()).getCodeLists().stream()
				.findFirst()
				.orElse(null);

		if (codeList == null) {
			return null;
		}

		return codeList.getCodedValues();
	}
}
