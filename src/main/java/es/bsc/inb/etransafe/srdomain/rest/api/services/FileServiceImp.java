package es.bsc.inb.etransafe.srdomain.rest.api.services;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonGenerationException;

@Service
public class FileServiceImp implements FileService {
  	//Nombre de la carpeta donde vamos a almacenar los archivos
    //Se crea a nivel de raiz la carpeta
    private final Path root = Paths.get("uploads");
    
    @Autowired
    MongoTemplate mongoTemplate;
    
    @Autowired
    DocumentService documentService;
    
    @Override
    public void init() {
        try {
            Files.createDirectory(root);
        } catch (IOException e) {
            throw new RuntimeException("No se puede inicializar la carpeta uploads");
        }
    }
    
    /**
	 * Execute process in a document
	 * @param file
	 * @param outputGATEFile
	 * @throws ResourceInstantiationException
	 * @throws IOException 
	 * @throws JsonGenerationException 
	 * @throws InvalidOffsetException
	 */
	public void save(MultipartFile file){
		try {
			String collectionName = "annotations";
			if(file.getOriginalFilename().endsWith("documents.json")) {
				collectionName = "documents";
			}
			ByteArrayInputStream stream = new   ByteArrayInputStream(file.getBytes());
			String jsonString = IOUtils.toString(stream, "UTF-8");
			Document doc = Document.parse(jsonString);
			mongoTemplate.save(doc, collectionName);
			doc.clear();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * return if document id exist in database.
	 * @param doc
	 */
    private Boolean documentIdExist(Document doc) {
		String documentId = doc.getString("id");
		es.bsc.inb.etransafe.srdomain.rest.api.model.Document  document = documentService.findById(documentId);
		if(document!=null) {
			return true;
		}
		return false;
	}

    /**
	 * Return if document name already exist in database.
	 * @param doc
	 */
    private Boolean documentNameExist(Document doc) {
		String name = doc.getString("name");
		List<es.bsc.inb.etransafe.srdomain.rest.api.model.Document>  documents = documentService.findByDocumentsName(name);
		if(documents!=null && documents.size()>0) {
			return true;
		}
		return false;
	}
    
	@Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if(resource.exists() || resource.isReadable()){
                return resource;
            }else{
                throw new RuntimeException("No se puede leer el archivo ");
            }

        }catch (MalformedURLException e){
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    @Override
    public Stream<Path> loadAll(){
        //Files.walk recorre nuestras carpetas (uploads) buscando los archivos
        // el 1 es la profundidad o nivel que queremos recorrer
        // :: Referencias a metodos
        // Relativize sirve para crear una ruta relativa entre la ruta dada y esta ruta
        try{
            return Files.walk(this.root,1).filter(path -> !path.equals(this.root))
                    .map(this.root::relativize);
        }catch (RuntimeException | IOException e){
            throw new RuntimeException("No se pueden cargar los archivos ");
        }
    }

    @Override
    public String deleteFile(String filename){
        try {
            Boolean delete = Files.deleteIfExists(this.root.resolve(filename));
                return "Borrado";
        }catch (IOException e){
            e.printStackTrace();
            return "Error Borrando ";
        }
    }

	@Override
	public List<String> validate(MultipartFile[] files, Boolean allow_duplicates) {
		List<String> errors = new ArrayList<String>();
		Map<String, MultipartFile[]> map = new HashMap<String, MultipartFile[]>();
		List<String> annotations_files = new ArrayList<String>();
		List<String> documents_files = new ArrayList<String>();
		List<MultipartFile> documents_files_multipart = new ArrayList<MultipartFile>();
		for (MultipartFile multipartFile : files) {
			if(multipartFile.getOriginalFilename().endsWith("_annotations.json")) {
				//annotations
				String file = multipartFile.getOriginalFilename().subSequence(0, multipartFile.getOriginalFilename().indexOf("_annotations.json")).toString();
				annotations_files.add(file);
			}else if(multipartFile.getOriginalFilename().endsWith("_documents.json")){ 
				//documents
				String file = multipartFile.getOriginalFilename().subSequence(0, multipartFile.getOriginalFilename().indexOf("_documents.json")).toString();
				documents_files.add(file);
				documents_files_multipart.add(multipartFile);
			}else {
				errors.add("The following file is not a valid format, it must be an 'xxxx_annotations.json' or 'xxxx_documents.json' format: " + multipartFile.getOriginalFilename() + ".");
			}
		}
		
		List<String> differences_1 = new ArrayList<>((CollectionUtils.removeAll(annotations_files, documents_files)));
		for (String string : differences_1) {
			errors.add("The following document is incomplete : " + string + ", please review that both files : "+ string +"_annotations.json and " + string + "_documents.json where included.");
		}
		List<String> differences_2 = new ArrayList<>((CollectionUtils.removeAll(documents_files,annotations_files)));
		for (String string : differences_2) {
			errors.add("The following document is incomplete : " + string + ", please review that both files : "+ string +"_annotations.json and " + string + "_documents.json where included.");
		}
		
		if(errors.size()==0) {
			errors.addAll(validateDocumentsInDatabase(Arrays.asList(files)));
			if(!allow_duplicates) {
				//solo se esta validando los _documents.json habria que tambien tener en cuenta los annotations.  
				errors.addAll(validateDocumentsNamesInDatabase(Arrays.asList(files)));
			}
		}
		
		
		
		
		return errors;
		
	}
	/**
	 * Validate if there the document is already in the database
	 * @param files
	 * @return
	 */
	private List<String> validateDocumentsInDatabase(List<MultipartFile> files) {
		Set<String> errors = new HashSet<String>();
		for (MultipartFile multipartFile : files) {
			try {
				ByteArrayInputStream stream = new   ByteArrayInputStream(multipartFile.getBytes());
				String jsonString = IOUtils.toString(stream, "UTF-8");
				Document doc = Document.parse(jsonString);
				if(this.documentIdExist(doc)) {
					//String name = multipartFile.getOriginalFilename().subSequence(0, multipartFile.getOriginalFilename().indexOf("_documents.json")).toString();
					String name = doc.getString("name");
					errors.add("The document " + name + " has a documentId ("+doc.getLong("id")+") that was already processed and is present in the database. A document with the same documentId cannot be processed again.");
				}
			} catch (IOException e) {
				errors.add("The document file " + multipartFile + " is not a valid json files for the PretoxTM system.");
			}
			
		}
		return new ArrayList<String>(errors);
	}
	/**
	 * Validate if there the is a document with the same name in the database
	 * @param files
	 * @return
	 */
	private List<String> validateDocumentsNamesInDatabase(List<MultipartFile> files) {
		Set<String> errors = new HashSet<String>();
		for (MultipartFile multipartFile : files) {
			try {
				ByteArrayInputStream stream = new   ByteArrayInputStream(multipartFile.getBytes());
				String jsonString = IOUtils.toString(stream, "UTF-8");
				Document doc = Document.parse(jsonString);
				if(this.documentNameExist(doc)) {
					//String name = multipartFile.getOriginalFilename().subSequence(0, multipartFile.getOriginalFilename().indexOf("_documents.json")).toString();
					String name = doc.getString("name");
					errors.add("The document with the name: " + name + " was already processed and is present in the database. If you want to process again please select the option 'Allowing duplicates'. You will have two documents with the same name, you can difference them throught the process date. You shoud use this option if you want to upload the same document with different pipelines executions." );
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return new ArrayList<String>(errors);
	}

}
