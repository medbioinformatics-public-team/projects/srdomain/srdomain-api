package es.bsc.inb.etransafe.srdomain.rest.api.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

/**
 * Tracking of SRDomain document. Not used right now. It could be usefull for a future requirement.
 * @author javi
 *
 */
@org.springframework.data.mongodb.core.mapping.Document(collection = "tracking_srdomain")
public class DocumentTracking {

	@Id
	private String documentId;

	List<Tracking> trackings;

	public DocumentTracking() {
		super();
	}

	public DocumentTracking(String documentId) {
		this.documentId = documentId;
		this.trackings = new ArrayList<Tracking>();
	}

	public void addTracking(Tracking tracking) {
		if (trackings == null) {
			trackings = new ArrayList<Tracking>();
		}

		trackings.add(tracking);
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public List<Tracking> getTrackings() {
		return trackings;
	}

	public void setTrackings(List<Tracking> trackings) {
		this.trackings = trackings;
	}
}