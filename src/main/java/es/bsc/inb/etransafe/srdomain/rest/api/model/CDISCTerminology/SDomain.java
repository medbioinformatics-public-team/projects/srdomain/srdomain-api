package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum SDomain implements ControlledTerms {
	 BW,
	 BG,
	 CL,
	 CV,
	 DD,
	 EG,
	 FW,
	 LB,
	 MA,
	 MI,
	 OM,
	 PP,
	 RE,
	 VS;
	
	@Override
	public String toName() {
		return this.name();
	}
}
