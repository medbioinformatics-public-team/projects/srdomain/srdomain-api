package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum Manifestation {
	I ("Increase"),
	D ("Decrease"),
	P ("Present"),
	A ("Absent"),
	C ("Change");
	
	private String displayName;
	
	Manifestation(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}
