package es.bsc.inb.etransafe.srdomain.rest.api.security.pkce;



import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {

    	httpSecurity.headers().frameOptions().sameOrigin();
    	httpSecurity
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/liveness").permitAll()
				.antMatchers(HttpMethod.GET, "/readiness").permitAll()
				//.antMatchers(HttpMethod.POST, "/reports/**").permitAll()
				//.antMatchers(HttpMethod.GET, "/reports/**").permitAll()
				//.antMatchers(HttpMethod.GET, "/list/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api-docs").permitAll()
				.antMatchers(HttpMethod.GET, "/api-docs/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api.html").permitAll()
				.antMatchers(HttpMethod.GET, "/swagger-ui/**").permitAll()
				.anyRequest().authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .csrf()
                .disable()
                .oauth2ResourceServer()
                .jwt();

    }
}
