package es.bsc.inb.etransafe.srdomain.rest.api.model;

import java.util.List;

import org.springframework.data.annotation.Id;

/**
 * The SR-Domain Report.
 * @author javi
 *
 */
@org.springframework.data.mongodb.core.mapping.Document(collection="reports")
public class Document{
	/**
	 * Identifier. Mongo. 
	 */
	@Id
	private String id;
	/**
	 * Study Id of the report.  It is basically the name.
	 */
	private String studyId;
	
	/**
	 * Date of processing
	 */
	private String processDate;
	
	/**
	 * Status of the report. Not used for now, it will be usefull in the future if the report has some status involved.
	 */
	private Status status;
	
	/**
	 * Findings del report
	 */
	private List<SRDomainFinding> srDomainFindings;
	
	/**
	 * State of the record. ACTIVE OR DELETED
	 */
	private RecordState recordState;
	
	/**
	 * External identifier of the pretox textual evidence. Can be used to map from srdomain to pretox textual evidence.
	 */
	private String pretoxReportId;
	
	public Document() {
		super();
	}

	public String getStudyId() {
		return studyId;
	}

	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Status getStatus() {
		if(status==null)return Status.DRAFT;
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getProcessDate() {
		return processDate;
	}

	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}


	public RecordState getRecordState() {
		return recordState;
	}

	public void setRecordState(RecordState recordState) {
		this.recordState = recordState;
	}
	
	public List<SRDomainFinding> getSrDomainFindings() {
		return srDomainFindings;
	}

	public void setSrDomainFindings(List<SRDomainFinding> srDomainFindings) {
		this.srDomainFindings = srDomainFindings;
	}

	public String getPretoxReportId() {
		return pretoxReportId;
	}

	public void setPretoxReportId(String pretoxReportId) {
		this.pretoxReportId = pretoxReportId;
	}
	
}
