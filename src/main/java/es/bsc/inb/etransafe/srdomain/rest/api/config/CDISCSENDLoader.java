package es.bsc.inb.etransafe.srdomain.rest.api.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mongodb.client.gridfs.model.GridFSFile;

import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCField;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodeList;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodedValue;
import es.bsc.inb.etransafe.srdomain.rest.api.repository.CDISCFieldRepository;
import es.bsc.inb.etransafe.srdomain.rest.api.util.Constants;

@Configuration
@EnableScheduling
public class CDISCSENDLoader {
	
	private static final String CDISCSENDURL = "https://evs.nci.nih.gov/ftp1/CDISC/SEND/SEND%20Terminology.odm.xml";
	
	private Document doc;
	
	private static final String CODE_LIST_FORMAT = "CL.(C[0-9]+).([A-Z]+)";
	
	@Autowired
	private CDISCFieldRepository cDISCFieldRepository;
	
	@Autowired
	private GridFsTemplate gridFsTemplate;
	
	private static final Log log = LogFactory.getLog(CDISCSENDLoader.class.getName());
	
	public void execute() {
		this.checkCDISCVersion();
	}
	
	@Scheduled(cron = "${CDISC.cron.expression}")
	private void checkCDISCVersion() {
		log.info("CDISCSENDLoader :: checkCDISCVersion ");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			DocumentBuilder docBuilder = dbf.newDocumentBuilder();
			URL url = new URL(CDISCSENDURL);
			InputStream stream = url.openStream();
			doc = docBuilder.parse(stream);
			String fileOID = doc.getElementsByTagName("ODM").item(0).getAttributes().getNamedItem("FileOID").getTextContent();
			GridFSFile gridFile = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(fileOID)));
			if (gridFile == null || !gridFile.getFilename().equals(fileOID)) {
				gridFsTemplate.store(url.openStream(), fileOID);
				loadCDISC(fileOID);
			}
		} catch (ParserConfigurationException | IOException | SAXException e) {
			log.error("Could not load new version of CDISC_SEND terminology");
			e.printStackTrace();
		}
	}
	
	private void loadCDISC(String fileOID) {
		log.info("Loading new version of CDISC_SEND terminology");
		NodeList nodelist = doc.getElementsByTagName("CodeList");
		List<CDISCField> fields = initializeFields();
		
		for (int i = 0; i < nodelist.getLength(); i++) {
			Node node = nodelist.item(i);
			String oid =  node.getAttributes().getNamedItem("OID").getTextContent();
			String codeListId =  node.getAttributes().getNamedItem("nciodm:ExtCodeID").getTextContent();
			String codeListLongName =  node.getAttributes().getNamedItem("Name").getTextContent();
			Pattern p = Pattern.compile(CODE_LIST_FORMAT);
			Matcher m = p.matcher(oid);
			if (!m.find()) {
				log.warn("Could not read " + oid);
				continue;
			}
			
			String oidName = m.group(2); 
			NodeList enumeratedItemNodes = ((Element) node).getElementsByTagName("EnumeratedItem");
			NodeList codeListDescriptionNodes = ((Element) node).getElementsByTagName("Description");
			
			fields.stream()
				.forEach(field -> {
					field.getCodeLists().stream()
					.filter(term -> term.getName().equals(oidName))
					.findFirst()
					.ifPresent((codeList) -> {
						String codeListDescription = codeListDescriptionNodes.item(0).getTextContent();
						codeList.setDescription(codeListDescription);
						codeList.setId(codeListId);
						codeList.setLongName(codeListLongName);
						for (int j = 0; j < enumeratedItemNodes.getLength(); j++) {
							Node enumeratedItem = enumeratedItemNodes.item(j);
							String codedValue = enumeratedItem.getAttributes().getNamedItem("CodedValue").getTextContent();
							String codedValueId = enumeratedItem.getAttributes().getNamedItem("nciodm:ExtCodeID").getTextContent();
							String codedValuePreferredTerm = ((Element) enumeratedItem).getElementsByTagName("nciodm:PreferredTerm").item(0).getTextContent();
							String codedValueDescription = ((Element) enumeratedItem).getElementsByTagName("nciodm:CDISCDefinition").item(0).getTextContent();
							codeList.getCodedValues().add(new CodedValue(codedValueId, codedValue, codedValuePreferredTerm, codedValueDescription));
						}
					});
				});
		}
		
		fields.stream()
			.forEach(field -> {
				CDISCField oldField = cDISCFieldRepository.findByName(field.getName());
				if (oldField != null) {
					field.set_id(oldField.get_id());
				}

				cDISCFieldRepository.save(field);
			});
		
		log.info("Loaded new version of CDISC SEND terminology (" + fileOID + ")");
	}

	private List<CDISCField> initializeFields() {
		return Constants.SR_FIELD_NAME_DESCRIPTIONS.entrySet().stream()
				.map(entry -> {
					String fieldName = entry.getKey().name();
					String description = entry.getValue();
					CDISCField field = new CDISCField(fieldName, description);
					Constants.SR_FIELD_CODELISTS.get(entry.getKey()).stream().forEach(codeListEnum -> {
						CodeList codeList = new CodeList(codeListEnum.toName());
						field.getCodeLists().add(codeList);
					});
					
					return field;
				})
				.collect(Collectors.toList());
	}
}
