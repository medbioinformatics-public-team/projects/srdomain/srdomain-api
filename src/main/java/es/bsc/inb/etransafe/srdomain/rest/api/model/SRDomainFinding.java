package es.bsc.inb.etransafe.srdomain.rest.api.model;

/**
 * This class describes a SR-Domain Finding. 
 * 
 * @author javi
 *
 */
public class SRDomainFinding implements Cloneable {
	
	/**
	 * Internal identifier of the SRDomain Finding ID
	 */
	private Integer srDomainId;
	
	/**
	 * All the SR-Domain pure atributes.
	 */
	private String STUDYID = ""; // Study Identifier
	private String DOMAIN = ""; // Domain Abbreviation
	private String SRSEQ = ""; // Sequence number
	private String SRRISK = ""; // Risk Level Associated with this Group/Sex
	private String SPGRPCD = ""; // Sponsor-Defined Group Code
	private String GRPLBL = ""; // Group Name grplbl
	private String SRGRPDOS = ""; // Group Dose Level
	private String SRSEX = ""; // Sex
	private String SRSTDY = ""; // Study Day of Start of Finding
	private String SRSTPHSE = ""; // Study Phase of first Observation
	private String SROBSTDY = ""; // Start Phase Day of Observation
	private String SRENDY = ""; // Study Day of End of Finding
	private String SRENPHSE = ""; // Study Phase of last Observation
	private String SROBENDY = ""; // End Phase Day of Observation
	private String SRDOMAIN = ""; // Domain of Finding
	private String SRSPEC = ""; // Specimen of Finding
	private String SRTSTCD = ""; // Test Short Name
	private String SRFNDG = ""; // Finding
	private String SRORES = ""; // Observation (original result)
	private String SROBSV = ""; // Manifestation of Finding
	private String SROBSQ = ""; // Observation Qualifier
	private String SRSEV = ""; // Severity of Finding
	private String SRPCNT = ""; // Scale of this Finding
	private String SRSIGF = ""; // Statistical Significance
	private String SRTRTEF = ""; // Treatment-Related?
	private String SRCOMNT = ""; // Comment

	/**
	 * this field is a copy of findingId. Is only used to map against the field present in srdomain-api.
	 */
	private Integer pretoxFindingId;
	
	public String getSTUDYID() {
		return STUDYID;
	}

	public void setSTUDYID(String sTUDYID) {
		STUDYID = sTUDYID;
	}

	public String getDOMAIN() {
		return DOMAIN;
	}

	public void setDOMAIN(String dOMAIN) {
		DOMAIN = dOMAIN;
	}

	public String getSRSEQ() {
		return SRSEQ;
	}

	public void setSRSEQ(String sRSEQ) {
		SRSEQ = sRSEQ;
	}

	public String getSRRISK() {
		return SRRISK;
	}

	public void setSRRISK(String sRRISK) {
		SRRISK = sRRISK;
	}

	public String getSPGRPCD() {
		return SPGRPCD;
	}

	public void setSPGRPCD(String sPGRPCD) {
		SPGRPCD = sPGRPCD;
	}

	public String getSRGRPDOS() {
		return SRGRPDOS;
	}

	public void setSRGRPDOS(String sRGRPDOS) {
		SRGRPDOS = sRGRPDOS;
	}

	public String getSRSEX() {
		return SRSEX;
	}

	public void setSRSEX(String sRSEX) {
		SRSEX = sRSEX;
	}

	public String getSRSTDY() {
		return SRSTDY;
	}

	public void setSRSTDY(String sRSTDY) {
		SRSTDY = sRSTDY;
	}

	public String getSRSTPHSE() {
		return SRSTPHSE;
	}

	public void setSRSTPHSE(String sRSTPHSE) {
		SRSTPHSE = sRSTPHSE;
	}

	public String getSROBSTDY() {
		return SROBSTDY;
	}

	public void setSROBSTDY(String sROBSTDY) {
		SROBSTDY = sROBSTDY;
	}

	public String getSRENDY() {
		return SRENDY;
	}

	public void setSRENDY(String sRENDY) {
		SRENDY = sRENDY;
	}

	public String getSRENPHSE() {
		return SRENPHSE;
	}

	public void setSRENPHSE(String sRENPHSE) {
		SRENPHSE = sRENPHSE;
	}

	public String getSROBENDY() {
		return SROBENDY;
	}

	public void setSROBENDY(String sROBENDY) {
		SROBENDY = sROBENDY;
	}

	public String getSRDOMAIN() {
		return SRDOMAIN;
	}

	public void setSRDOMAIN(String sRDOMAIN) {
		SRDOMAIN = sRDOMAIN;
	}

	public String getSRSPEC() {
		return SRSPEC;
	}

	public void setSRSPEC(String sRSPEC) {
		SRSPEC = sRSPEC;
	}

	public String getSRTSTCD() {
		return SRTSTCD;
	}

	public void setSRTSTCD(String sRTSTCD) {
		SRTSTCD = sRTSTCD;
	}

	public String getSRFNDG() {
		return SRFNDG;
	}

	public void setSRFNDG(String sRFNDG) {
		SRFNDG = sRFNDG;
	}

	public String getSRORES() {
		return SRORES;
	}

	public void setSRORES(String sRORES) {
		SRORES = sRORES;
	}

	public String getSROBSV() {
		return SROBSV;
	}

	public String getSROBSQ() {
		return SROBSQ;
	}

	public void setSROBSQ(String sROBSQ) {
		SROBSQ = sROBSQ;
	}

	public String getSRSEV() {
		return SRSEV;
	}

	public void setSRSEV(String sRSEV) {
		SRSEV = sRSEV;
	}

	public String getSRPCNT() {
		return SRPCNT;
	}

	public void setSRPCNT(String sRPCNT) {
		SRPCNT = sRPCNT;
	}

	public String getSRSIGF() {
		return SRSIGF;
	}

	public void setSRSIGF(String sRSIGF) {
		SRSIGF = sRSIGF;
	}

	public String getSRTRTEF() {
		return SRTRTEF;
	}

	public void setSRTRTEF(String sRTRTEF) {
		SRTRTEF = sRTRTEF;
	}

	public String getSRCOMNT() {
		return SRCOMNT;
	}

	public void setSRCOMNT(String sRCOMNT) {
		SRCOMNT = sRCOMNT;
	}

	public Integer getSrDomainId() {
		return srDomainId;
	}

	public void setSrDomainId(Integer srDomainId) {
		this.srDomainId = srDomainId;
	}
    
	

	public Integer getPretoxFindingId() {
		return pretoxFindingId;
	}

	public void setPretoxFindingId(Integer pretoxFindingId) {
		this.pretoxFindingId = pretoxFindingId;
	}

	public SRDomainFinding clone() {
		try {
			return (SRDomainFinding) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	

	public String getGRPLBL() {
		return GRPLBL;
	}

	public void setGRPLBL(String gRPLBL) {
		GRPLBL = gRPLBL;
	}

	public void setSROBSV(String sROBSV) {
		SROBSV = sROBSV;
	}
	
	

}
