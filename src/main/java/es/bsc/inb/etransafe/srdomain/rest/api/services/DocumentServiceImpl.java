package es.bsc.inb.etransafe.srdomain.rest.api.services;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.line.LineStyle;
import es.bsc.inb.etransafe.srdomain.rest.api.model.Action;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodedValue;
import es.bsc.inb.etransafe.srdomain.rest.api.model.Document;
import es.bsc.inb.etransafe.srdomain.rest.api.model.DocumentTracking;
import es.bsc.inb.etransafe.srdomain.rest.api.model.RecordState;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportEffectCause;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportObservationOrdering;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportSpecimenOrdering;
import es.bsc.inb.etransafe.srdomain.rest.api.model.SRDomainFinding;
import es.bsc.inb.etransafe.srdomain.rest.api.model.Status;
import es.bsc.inb.etransafe.srdomain.rest.api.model.Tracking;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Manifestation;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Risk;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.SDomain;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Sex;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.TreatmentRelated;
import es.bsc.inb.etransafe.srdomain.rest.api.repository.DocumentRepository;
import es.bsc.inb.etransafe.srdomain.rest.api.repository.DocumentTrackingRepository;

@Service
public class DocumentServiceImpl implements DocumentService {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	public DocumentRepository documentRepository;

	@Autowired
	public DocumentTrackingRepository documentTrackingRepository;
	
	@Autowired
	private CDISCService cdiscService;
	
	private static final Log log = LogFactory.getLog(DocumentServiceImpl.class.getName());

	public List<Document> findAll() {
		return documentRepository.findAllStudyIds();
	}

	public Document findById(String id) {
		return documentRepository.findById(id).orElse(null);
	}

	@Override
	public Document save(Document document) {
		return documentRepository.save(document);
	}
	
	@Override
	public Boolean validateCreate(Document document) {
		List<Document> reports = this.findByDocumentsName(document.getStudyId());
		boolean isRepeated = reports != null && reports.stream().anyMatch(report -> report.getRecordState() != RecordState.DELETED);
		return !isRepeated;
	}
	
	@Override
	public Boolean existDocumentByStudyId(String studyId) {
		return this.findByDocumentsName(studyId)!=null && this.findByDocumentsName(studyId).size() > 0;
	}
	
	public List<Document> findByDocumentsName(String studyId) {
		return documentRepository.findByStudyId(studyId);
	}

	@Override
	public Status moveDocument(String id, Status status) {
		Document document = this.findById(id);
		document.setStatus(status);
		documentRepository.save(document);
		createTracking(id, Action.MOVE, status.toString());
		return status;
	}

	private void createTracking(String id, Action action, String comment) {
		DocumentTracking documentTracking = this.findDocumentTrackingById(id);
		Tracking tracking = new Tracking(getUserName(), action, comment);
		documentTracking.addTracking(tracking);
		documentTrackingRepository.save(documentTracking);
	}

	private DocumentTracking findDocumentTrackingById(String id) {
		DocumentTracking documentTracking = documentTrackingRepository.findByDocumentId(id);
		if (documentTracking == null) {
			documentTracking = new DocumentTracking(id);
		}
		
		return documentTracking;
	}

	public DocumentTracking findTrackings(String documentId) {
		DocumentTracking documentTracking = this.findDocumentTrackingById(documentId);
		return documentTracking;
	}

	@Override
	public void removeDocument(String documentId) {
		Document document = this.findById(documentId);
		document.setRecordState(RecordState.DELETED);
		documentRepository.save(document);
	}

	@Override
	public void restoreDocument(String documentId) {
		Document document = this.findById(documentId);
		document.setRecordState(RecordState.ACTIVE);
		documentRepository.save(document);
	}

	private String getUserName() {
		Principal user = request.getUserPrincipal();
		String userName = "";
		if (user != null && user instanceof JwtAuthenticationToken) {
			JwtAuthenticationToken token = (JwtAuthenticationToken) user;
			if (token.getTokenAttributes().get("name") != null) {
				userName = token.getTokenAttributes().get("name").toString();
			}
		}

		return userName;
	}
	
	/**
	 * Export SR-Domain format marked as exportable.
	 */
	public Map<String, Object> exportToSRDomainFile(String id) {
		Document document = this.findById(id);
		StringBuilder retStr = new StringBuilder("");
		retStr.append("﻿STUDYID|DOMAIN|SRSEQ|SRRISK|SPGRPCD|GRPLBL|SRGRPDOS|SRSEX|SRSTDY|SRSTPHSE|SROBSTDY|SRENDY|SRENPHSE|SROBENDY|SRDOMAIN|SRSPEC|SRTSTCD|SRFNDG|SRORES|SROBSV|SROBSQ|SRSEV|SRPCNT|SRSIGF|SRTRTEF|SRCOMNT");
		retStr.append(System.getProperty("line.separator"));
		for (SRDomainFinding srDomainFinding : document.getSrDomainFindings()) {
			//STUDYID -- > Study Identifier  
			retStr.append(srDomainFinding.getSTUDYID());
			retStr.append("|");
			//DOMAIN  -- > Domain Abbreviation, always SR 
			retStr.append("SR");
			retStr.append("|");
			//SRSEQ  -- > Sequence number
			retStr.append(srDomainFinding.getSRSEQ());
			retStr.append("|");
			//SRRISK --> Effect Level Associated with a Group/Sex, NOEL, LOEL, NOAEL, LOAEL, HNSTD, STD, MTD 
			retStr.append(srDomainFinding.getSRRISK()==null?"":srDomainFinding.getSRRISK());
			retStr.append("|");
			//SPGRPCD --> Sponsor-defined Group Code, group 1 , A 
			retStr.append(srDomainFinding.getSPGRPCD()==null?"":srDomainFinding.getSPGRPCD());
			retStr.append("|");
			//GRPLBL --> Sponsor-defined Group Name, "low dose", "mid dose"
			retStr.append(srDomainFinding.getGRPLBL()==null?"":srDomainFinding.getGRPLBL());
			retStr.append("|");
			//SRGRPDOS --> Group Dose Level, "20 mg/kg"
			retStr.append(srDomainFinding.getSRGRPDOS()==null?"":srDomainFinding.getSRGRPDOS());
			retStr.append("|");
			//SRSEX -- > Sex
			retStr.append(srDomainFinding.getSRSEX()==null?"":srDomainFinding.getSRSEX());
			retStr.append("|");
			//SRSTDY --> Study Day of Start of Finding
			retStr.append("");
			retStr.append("|");
			//SRSTPHSE --> Study Phase of first Observation, “PRE-DOSING”, “DOSING”, “RECOVERY”
			retStr.append("");
			retStr.append("|");
			//SROBSTDY --> Start Phase Day of Observation
			retStr.append("");
			retStr.append("|");
			//SRENDY --> ??
			retStr.append("");
			retStr.append("|");
			//SRENPHSE -->Study Phase of last Observation
			retStr.append("");
			retStr.append("|");
			//SROBENDY --> ??
			retStr.append("");
			retStr.append("|");
			//SRDOMAIN --> Domain of Finding
			retStr.append(srDomainFinding.getSRDOMAIN()==null?"":srDomainFinding.getSRDOMAIN());
			retStr.append("|");
			//SRSPEC --> Specimen of Finding
			retStr.append(srDomainFinding.getSRSPEC()==null?"":srDomainFinding.getSRSPEC());
			retStr.append("|");
			//SRTSTCD --> Test Short Name
			retStr.append(srDomainFinding.getSRTSTCD()==null?"":srDomainFinding.getSRTSTCD());
			retStr.append("|");
			//SRFNDG --> Finding
			retStr.append(srDomainFinding.getSRFNDG()==null?"":srDomainFinding.getSRFNDG());
			retStr.append("|");
			//SRORES --> Observation (original result)
			retStr.append(srDomainFinding.getSRORES()==null?"":srDomainFinding.getSRORES());
			retStr.append("|");
			//SROBSV --> Manifestation of Finding, “I” (Increase), “D” (Decrease), “P” (Present) or “A” (Absent)
			retStr.append(srDomainFinding.getSROBSV()==null?"":srDomainFinding.getSROBSV());
			retStr.append("|");
			//SROBSQ --> Observation Qualifier, “R” (Reversible), “T” (Transient) or ‘none’
			retStr.append(srDomainFinding.getSROBSQ()==null?"":srDomainFinding.getSROBSQ());
			retStr.append("|");
			//SRSEV --> Severity of Finding
			retStr.append("");
			retStr.append("|");
			//SRPCNT --> Scale of this Finding
			retStr.append("");
			retStr.append("|");
			//SRSIGF --> Statistical Significance
			retStr.append(srDomainFinding.getSRSIGF()==null?"":srDomainFinding.getSRSIGF());
			retStr.append("|");
			//SRTRTEF --> Treatment-Related
			retStr.append(srDomainFinding.getSRTRTEF()==null?"":srDomainFinding.getSRTRTEF());
			retStr.append("|");
			//SRCOMNT --> Comment
			retStr.append(" \n");
		}
		Map<String, Object> file_data = new HashMap<String, Object>();
		file_data.put("file_name", document.getStudyId());
		file_data.put("content", retStr.toString().getBytes(Charset.forName("UTF-8")));
		return file_data;
	}


	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public List<String> importSRDomainValidation(MultipartFile[] files) {
		List<String> errors = new ArrayList<String>();
		for (MultipartFile multipartFile : files) {
			String studyReportId = multipartFile.getOriginalFilename().replace(".csv", "");
			if(existDocumentByStudyId(studyReportId)) {
				errors.add("There is a study report with the name: " + studyReportId + " in the database. " );
			}
		}
		return errors;
	}
		
	@Override
	/**
	 * Create/Import SRDomain from SRDomain file format into SRDomain document and stored into the database.
	 */
	public Document importSRDomain(MultipartFile file) {
		try {
			log.info("DocumentServiceImpl :: importSRDomain :: file: " + file.getName());
			InputStream inputStream = file.getInputStream();
			List<String> srdomain_document_format = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
			//0 index contains the names of the columns
			srdomain_document_format.remove(0);
			Document document = new Document();
			document.setSrDomainFindings(new ArrayList<SRDomainFinding>());
			String studyId = "";
			for (String line : srdomain_document_format) {
				String[] data = line.split("\\|"); 
				SRDomainFinding finding = new SRDomainFinding();
				studyId=data[0];
				finding.setSTUDYID(data[0]);
				finding.setDOMAIN(data[1]);
				finding.setSRSEQ(data[2]);
				finding.setSRRISK(data[3]);
				finding.setSPGRPCD(data[4]);
				finding.setGRPLBL(data[5]);
				finding.setSRGRPDOS(data[6]);
				finding.setSRSEX(data[7]);
				finding.setSRSTDY(data[8]);
				finding.setSRSTPHSE(data[9]);
				finding.setSROBSTDY(data[10]);
				finding.setSRENDY(data[11]);
				finding.setSRENPHSE(data[12]);
				finding.setSROBENDY(data[13]);
				finding.setSRDOMAIN(data[14]);
				finding.setSRSPEC(data[15]);
				finding.setSRTSTCD(data[16]);
				finding.setSRFNDG(data[17]);
				finding.setSRORES(data[18]);
				finding.setSROBSV(data[19]);
				finding.setSROBSQ(data[20]);
				finding.setSRSEV(data[21]);
				finding.setSRPCNT(data[22]);
				finding.setSRSIGF(data[23]);
				finding.setSRTRTEF(data[24]);
				//If there is an error comment is ""
				try {
					finding.setSRCOMNT(data[25]);
				}catch(IndexOutOfBoundsException e) {
					finding.setSRCOMNT("");
				}
				document.getSrDomainFindings().add(finding);
			}
			document.setStudyId(studyId);
			this.save(document);
			log.info("SRDomain report file imported correctly");
			return document;
		} catch (Exception e) {
			log.error("An error ocurred importing the SRDomain report with file name :" + file.getName(), e);
			return null;
		}
	}
	
	private BaseTable generateDoseGroupTable(PDDocument pdfDoc, PDPage pdfPage, List<SRDomainFinding> srDomainFindings, float yStart, float margin) throws IOException {
	    float yStartNewPage = pdfPage.getMediaBox().getHeight() - (2 * margin);
	    float tableWidth = (pdfPage.getMediaBox().getWidth() - (2 * margin)) / 2;
	    boolean drawContent = true;
	    float bottomMargin = 70;
	    
	    List<String> columnTitles = new ArrayList<>(Arrays.asList("Group ID", "Group Name", "Group Dosage"));
	    float cellWidthPerc = 100/(columnTitles.size());
	    BaseTable table = new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, margin, pdfDoc, pdfPage, true, drawContent);
	    Row<PDPage> headerRow = table.createRow(15);
	    for (int i = 0; i < columnTitles.size(); i++) {
	    	Cell<PDPage> cell = headerRow.createCell(cellWidthPerc, columnTitles.get(i));
		    cell.setFont(PDType1Font.HELVETICA_BOLD);
		    cell.setFontSize(10);
		    cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
	    }
	    
	    table.addHeaderRow(headerRow);
	    
	    Map<String, List<SRDomainFinding>> groupCodesToFindings = srDomainFindings.stream()
	    	.collect(Collectors.groupingBy(SRDomainFinding::getSPGRPCD));
	    
	    groupCodesToFindings.forEach((code, findings) -> {
	    	SRDomainFinding sampleFinding = findings.get(0); // all findings in each group are supposed to have the same values for these fields
	    	Row<PDPage> row = table.createRow(15);
		    row.createCell(cellWidthPerc, code);
		    row.createCell(cellWidthPerc, sampleFinding.getGRPLBL());
		    row.createCell(cellWidthPerc, sampleFinding.getSRGRPDOS());
		    row.getCells().forEach(cell -> {
		    	cell.setFontSize(10);
		    	cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
		    });
	    });
	    
	    return table;
	}
	
	private BaseTable generateDomainTable(PDDocument pdfDoc, PDPage pdfPage, List<SRDomainFinding> srDomainFindings, float yStart, float margin) throws IOException {
		// starting y position is whole page height subtracted by top and bottom margin
	    float yStartNewPage = pdfPage.getMediaBox().getHeight() - (2 * margin);
	    // we want table across whole page width (subtracted by left and right margin ofcourse)
	    float tableWidth = pdfPage.getMediaBox().getWidth() - (2 * margin);
	    boolean drawContent = true;
	    float bottomMargin = 70;
	    
	    List<String> columnTitles = new ArrayList<>(Arrays.asList("Domain ID", "Domain Name", "Sex", "Start Phase", "Phase Day",
	    		"Study Day", "End Phase", "Phase Day", "Study Day"));
	    
	    float cellWidthPerc = 100/(columnTitles.size());
	    
	    BaseTable table = new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, margin, pdfDoc, pdfPage, true, drawContent);
	    Row<PDPage> headerRow = table.createRow(15);
	    for (int i = 0; i < columnTitles.size(); i++) {
	    	Cell<PDPage> cell = headerRow.createCell(cellWidthPerc, columnTitles.get(i));
		    cell.setFont(PDType1Font.HELVETICA_BOLD);
		    cell.setFontSize(10);
		    cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
	    }
	    
	    table.addHeaderRow(headerRow);
	    
	    Map<String, Map<String, Map<String, Map<String, Map<String, Map<String, Map<String, Map<String, List<SRDomainFinding>>>>>>>>> domainGroups = srDomainFindings.stream()
		    	.filter(finding -> !finding.getSRDOMAIN().equals(""))
	    		.collect(Collectors.groupingBy(SRDomainFinding::getSRDOMAIN,														// domain name
		    			Collectors.groupingBy(SRDomainFinding::getSRSEX,															// sex
		    					Collectors.groupingBy(SRDomainFinding::getSRSTPHSE,													// start phase
		    							Collectors.groupingBy(SRDomainFinding::getSROBSTDY,											// start phase day
		    									Collectors.groupingBy(SRDomainFinding::getSRSTDY,									// start study day
		    											Collectors.groupingBy(SRDomainFinding::getSRENPHSE,							// end phase
		    													Collectors.groupingBy(SRDomainFinding::getSROBENDY,					// end phase day
		    															Collectors.groupingBy(SRDomainFinding::getSRENDY)))))))));	// end study day
	    AtomicInteger seqNum =  new AtomicInteger();
	    List<CodedValue> codedValues = cdiscService.findAllStudyDomainsCodedValues();
	    domainGroups.forEach((domain, sexGroups) -> {
	    	seqNum.set(1);
	    	sexGroups.forEach((sex, startPhaseGroups) -> {
	    		startPhaseGroups.forEach((startPhase, startPhaseDayGroups) -> {
	    			startPhaseDayGroups.forEach((startPhaseDay, startStudyDayGroups) -> {
	    				startStudyDayGroups.forEach((startStudyDay, endPhaseGroups) -> {
	    					endPhaseGroups.forEach((endPhase, endPhaseDayGroups) -> {
	    						endPhaseDayGroups.forEach((endPhaseDay, endStudyDayGroups) -> {
	    							endStudyDayGroups.forEach((endStudyDay, findings) -> {
	    								Row<PDPage> row = table.createRow(15);
	    								CodedValue domainCodedValue = codedValues.stream().filter(domainCV -> domainCV.getName().equals(domain)).findAny().orElse(null);
    								    String domainName = domainCodedValue == null
    								    		? ""
    								    		: domainCodedValue.getPreferredTerm().replace(" Domain", "");
    								    String domainCode = domain;
    								    
	    								if (sexGroups.keySet().size() > 1) {
	    									domainCode = domain + " (" + seqNum.getAndIncrement() + ")";
	    								}
	    								
    								    row.createCell(cellWidthPerc, domainCode);
    								    row.createCell(cellWidthPerc, domainName);
    								    String sexName = EnumUtils.isValidEnum(Sex.class, sex)
    								    		? Sex.valueOf(sex).equals(Sex.B)
    								    			? "Male & Female"
    								    			: Sex.valueOf(sex).getDisplayName()
    								    		: "";
    								    row.createCell(cellWidthPerc, sexName);
    								    row.createCell(cellWidthPerc, startPhase);
    								    row.createCell(cellWidthPerc, startPhaseDay);
    								    row.createCell(cellWidthPerc, startStudyDay);
    								    row.createCell(cellWidthPerc, endPhase);
    								    row.createCell(cellWidthPerc, endPhaseDay);
    								    row.createCell(cellWidthPerc, endStudyDay);
    								    row.getCells().forEach(cell -> {
    								    	cell.setFontSize(10);
    								    	cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
    								    });
	    							});
	    						});
	    					});
	    				});
	    			});
	    		});
	    	});
	    });
	    
	    return table;
	}
	
	private BaseTable generateEffectLevelTable(PDDocument pdfDoc, PDPage pdfPage, List<SRDomainFinding> srDomainFindings, float yStart, float margin) throws IOException {
		// starting y position is whole page height subtracted by top and bottom margin
	    float yStartNewPage = pdfPage.getMediaBox().getHeight() - (2 * margin);
	    // we want table across whole page width (subtracted by left and right margin ofcourse)
	    float tableWidth = pdfPage.getMediaBox().getWidth() - (2 * margin);
	    boolean drawContent = true;
	    float bottomMargin = 70;
	    // y position is your coordinate of top left corner of the table
	    //float yPosition = 550;
	    
	    List<String> columnTitles = new ArrayList<>(Arrays.asList("Effect Level", "Group ID", "Sex", "Comment"));
	    
	    float cellWidthPerc = 100 / 9;
	    float commentCellWidthPerc = 100 * 6 / 9;
	    
	    BaseTable table = new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, margin, pdfDoc, pdfPage, true, drawContent);
	    Row<PDPage> headerRow = table.createRow(15);
	    columnTitles.forEach(columnTitle -> {
	    	float cellWidth = cellWidthPerc;
	    	if (columnTitle.equals("Comment")) {
	    		cellWidth = commentCellWidthPerc;
	    	}
	    	
	    	Cell<PDPage> cell = headerRow.createCell(cellWidth, columnTitle);
		    cell.setFont(PDType1Font.HELVETICA_BOLD);
		    cell.setFontSize(10);
		    cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
	    });
	    
	    table.addHeaderRow(headerRow);
	    
	    srDomainFindings.stream()
	    	.filter(finding -> finding.getSRRISK()!= null && !finding.getSRRISK().isEmpty())
	    	.forEach(finding -> {
	    		Row<PDPage> row = table.createRow(15);
	    		String riskName = EnumUtils.isValidEnum(Risk.class, finding.getSRRISK())
			    		? Risk.valueOf(finding.getSRRISK()).getDisplayName()
			    		: "";
			    row.createCell(cellWidthPerc, riskName);
			    row.createCell(cellWidthPerc, finding.getSPGRPCD());
			    String sexName = EnumUtils.isValidEnum(Sex.class, finding.getSRSEX())
			    		? Sex.valueOf(finding.getSRSEX()).equals(Sex.B)
			    			? "Male & Female"
			    			: Sex.valueOf(finding.getSRSEX()).getDisplayName()
			    		: "";
			    row.createCell(cellWidthPerc, sexName);
			    row.createCell(cellWidthPerc, finding.getSRCOMNT());
			    row.getCells().forEach(cell -> {
			    	cell.setFontSize(10);
			    	cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
			    });
	    	});
		    
	    return table;
	}
	
	private float drawDomainSexTables(PDDocument pdfDoc, PDPage pdfPage, PDPageContentStream content, List<SRDomainFinding> srDomainFindings,
			float tableXStart, float yStart, float titleYOffset, float margin, PDFont tableTitleFont, int tableTitleFontSize, 
			ReportSpecimenOrdering reportSpecimenOrdering, ReportEffectCause reportEffectCause, String reportDomain) throws IOException {
	    Map<String, Map<String, List<SRDomainFinding>>> domainGroups = srDomainFindings.stream()
	    		.filter(finding -> !finding.getSRDOMAIN().trim().equals(""))
	    		.filter(finding -> reportDomain == null || finding.getSRDOMAIN().equals(reportDomain))
	    		.filter(finding -> reportEffectCause == ReportEffectCause.ALL_EFFECTS ||
	    			(finding.getSRTRTEF() != null && finding.getSRTRTEF().equals(reportEffectCause.getSrDomainName())))
		    	.collect(Collectors.groupingBy(SRDomainFinding::getSRDOMAIN,
		    			Collectors.groupingBy(SRDomainFinding::getSRSEX)));			
	    
	    float tableTitleMargin = 10;
	    float tableYStart = yStart;
	    float tableEnd = 0;
	    float newLineOffset = 0;
	    
	    List<CodedValue> codedValues = cdiscService.findAllStudyDomainsCodedValues();
	    AtomicInteger seqNum =  new AtomicInteger();
	    
	    for (Map.Entry<String, Map<String, List<SRDomainFinding>>> domainGroup : domainGroups.entrySet()) {
	    	String domain = domainGroup.getKey();
	    	seqNum.set(1);
	    	Map<String, List<SRDomainFinding>> sexGroups = domainGroup.getValue();
	    	for (Map.Entry<String, List<SRDomainFinding>> sexGroup : sexGroups.entrySet()) {
	    		List<SRDomainFinding> findings = sexGroup.getValue();
	    		String sex = sexGroup.getKey();
			    CodedValue domainCodedValue = codedValues.stream().filter(domainCV -> domainCV.getName().equals(domain)).findAny().orElse(null);
			    String domainName = domainCodedValue == null
			    		? domain
			    		: domainCodedValue.getPreferredTerm().replace(" Domain", "") + " (" + domain + ")";
			    String sexName = EnumUtils.isValidEnum(Sex.class, sex)
			    		? Sex.valueOf(sex).getDisplayName()
			    		: "";
			    
			    if (sexGroups.keySet().size() > 1) {
			    	domainName = domainName + " (" + seqNum.getAndIncrement() + ")";
				}
			    
			    if (sexName.equals("Both")) {
			    	sexName = "Male";
			    	BaseTable table = this.generateDomainSexTable(pdfDoc, pdfPage, findings, tableYStart, margin, reportSpecimenOrdering, domain);
					String tableTitle = "Domain: " + domainName + "; Sex = " + sexName;
					table.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, table.getWidth(), 20, "left", 100, false);
					tableEnd = table.draw();
					
					if (table.getCurrentPage() != pdfPage) {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						content.endText();
						content.close();
						pdfPage = table.getCurrentPage();
						content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
						content.beginText();
						content.setFont(tableTitleFont, tableTitleFontSize);
						newLineOffset += tableTitleMargin;
						titleYOffset = tableEnd - margin + tableTitleMargin;
						tableXStart = margin;
					} else {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						titleYOffset = -Math.abs(tableEnd - tableYStart) - margin;
						tableXStart = 0;
					}
					
					tableYStart = tableEnd - margin;
					
					sexName = "Female";
			    	table = this.generateDomainSexTable(pdfDoc, pdfPage, findings, tableYStart, margin, reportSpecimenOrdering, domain);
					tableTitle = "Domain: " + domainName + "; Sex = " + sexName;
					table.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, table.getWidth(), 20, "left", 100, false);
					tableEnd = table.draw();
					
					if (table.getCurrentPage() != pdfPage) {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						content.endText();
						content.close();
						pdfPage = table.getCurrentPage();
						content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
						content.beginText();
						content.setFont(tableTitleFont, tableTitleFontSize);
						newLineOffset += tableTitleMargin;
						titleYOffset = tableEnd - margin + tableTitleMargin;
						tableXStart = margin;
					} else {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						titleYOffset = -Math.abs(tableEnd - tableYStart) - margin;
						tableXStart = 0;
					}
					
					tableYStart = tableEnd - margin;
			    } else {
			    	BaseTable table = this.generateDomainSexTable(pdfDoc, pdfPage, findings, tableYStart, margin, reportSpecimenOrdering, domain);
					String tableTitle = "Domain: " + domainName + "; Sex = " + sexName;
					table.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, table.getWidth(), 20, "left", 100, false);
					tableEnd = table.draw();
					
					if (table.getCurrentPage() != pdfPage) {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						content.endText();
						content.close();
						pdfPage = table.getCurrentPage();
						content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
						content.beginText();
						content.setFont(tableTitleFont, tableTitleFontSize);
						newLineOffset += tableTitleMargin;
						titleYOffset = tableEnd - margin + tableTitleMargin;
						tableXStart = margin;
					} else {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						titleYOffset = -Math.abs(tableEnd - tableYStart) - margin;
						tableXStart = 0;
					}
					
					tableYStart = tableEnd - margin;
			    }
	    	}
	    }
	    
	    content.endText();
		content.close();
	    
	    return tableYStart;
	}
	
	private BaseTable generateDomainSexTable(PDDocument pdfDoc, PDPage pdfPage, List<SRDomainFinding> srDomainFindings, float yStart,
			float margin, ReportSpecimenOrdering reportSpecimenOrdering, String domain) throws IOException {
		// starting y position is whole page height subtracted by top and bottom margin
	    float yStartNewPage = pdfPage.getMediaBox().getHeight() - (2 * margin);
	    // we want table across whole page width (subtracted by left and right margin of course)
	    float tableWidth = pdfPage.getMediaBox().getWidth() - (2 * margin);
	    boolean drawContent = true;
	    float bottomMargin = 70;
	    
	    List<String> columnTitles = new ArrayList<>(Arrays.asList("Specimen/   Body System", "Test/Finding/ Category", "Original Result", "Group", "Manifes-tation",
	    		"Severity", "% Inci-dence", "Qualifier", "Signifi-cance", "Treatment Related?", "Comment"));
	    
	    float cellWidthPerc = 100/(columnTitles.size());
	    float specimenCellWidthPerc = cellWidthPerc + 2.25f;
	    float groupCellWidthPerc = cellWidthPerc - 3;
	    float manifestationCellWidthPerc = cellWidthPerc - 1.75f;
	    float commentCellWidthPerc = 100 - 3 * specimenCellWidthPerc - 3 * groupCellWidthPerc - 2 * manifestationCellWidthPerc - 2 * cellWidthPerc;
	    
	    BaseTable table = new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, margin, pdfDoc, pdfPage, true, drawContent);
		Row<PDPage> headerRow = table.createRow(15);
		columnTitles.forEach(columnTitle -> {
			float cellWidth = cellWidthPerc;
			if (columnTitle.equals("Specimen/   Body System") || columnTitle.equals("Test/Finding/ Category") || columnTitle.equals("Original Result")) {
				cellWidth = specimenCellWidthPerc;
			} else if (columnTitle.equals("Group") || columnTitle.equals("% Inci-dence") || columnTitle.equals("Signifi-cance")) {
				cellWidth = groupCellWidthPerc;
			} else if (columnTitle.equals("Manifes-tation") || columnTitle.equals("Severity")) {
				cellWidth = manifestationCellWidthPerc;
			} else if (columnTitle.equals("Comment")) {
				cellWidth = commentCellWidthPerc;
			}
			
			Cell<PDPage> cell = headerRow.createCell(cellWidth, columnTitle);
		    cell.setFont(PDType1Font.HELVETICA_BOLD);
		    cell.setFontSize(10);
		    cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
		});
	    
	    table.addHeaderRow(headerRow);
	    Map<String, Map<String, List<SRDomainFinding>>> findingGroups = new HashMap<>();
	    if (reportSpecimenOrdering == ReportSpecimenOrdering.group_domain_with_specimen) {
	    	findingGroups = srDomainFindings.stream()
			    	.collect(Collectors.groupingBy(SRDomainFinding::getSRSPEC,
			    			Collectors.groupingBy(SRDomainFinding::getSPGRPCD)));
	    } else if (reportSpecimenOrdering == ReportSpecimenOrdering.specimen_with_group_domain) {
	    	findingGroups = srDomainFindings.stream()
			    	.collect(Collectors.groupingBy(SRDomainFinding::getSPGRPCD,
			    			Collectors.groupingBy(SRDomainFinding::getSRSPEC)));
	    }
	    
	    List<CodedValue> codedValues = cdiscService.findTestCdCodedValuesByDomain(SDomain.valueOf(domain));
	    findingGroups.values().stream()
	    	.forEach(findingGroup -> {
	    		findingGroup.values().stream()
	    			.forEach(findingsubGroup -> {
	    				findingsubGroup.forEach(finding -> {
	    					Row<PDPage> row = table.createRow(15);
	    				    row.createCell(specimenCellWidthPerc, finding.getSRSPEC());
	    				    String testOrFinding = (finding.getSRFNDG() != null && !finding.getSRFNDG().trim().equals(""))
	    				    		? finding.getSRFNDG()
	    				    		: finding.getSRTSTCD();
	    				    CodedValue studyTestCodedValue = codedValues != null
	    				    		? codedValues.stream().filter(studyTestCV -> studyTestCV.getName().equals(testOrFinding)).findAny().orElse(null)
	    				    		: null;
	    				    String testOrFindingName = studyTestCodedValue == null
	    				    		? testOrFinding
	    				    		: studyTestCodedValue.getPreferredTerm();
	    				    row.createCell(specimenCellWidthPerc, testOrFindingName);
	    				    row.createCell(specimenCellWidthPerc, finding.getSRORES());
	    				    row.createCell(groupCellWidthPerc, finding.getSPGRPCD());
	    				    String manifestation = EnumUtils.isValidEnum(Manifestation.class, finding.getSROBSV())
						    		? Manifestation.valueOf(finding.getSROBSV()).getDisplayName()
						    		: "";
	    				    row.createCell(manifestationCellWidthPerc, manifestation);
	    				    row.createCell(manifestationCellWidthPerc, finding.getSRSEV());
	    				    row.createCell(groupCellWidthPerc, finding.getSRPCNT());
	    				    row.createCell(cellWidthPerc, finding.getSROBSQ());
	    				    row.createCell(groupCellWidthPerc, finding.getSRSIGF());
	    				    String treatmentRelated = EnumUtils.isValidEnum(TreatmentRelated.class, finding.getSRTRTEF())
						    		? TreatmentRelated.valueOf(finding.getSRTRTEF()).getDisplayName()
						    		: "";
	    				    row.createCell(cellWidthPerc, treatmentRelated);
	    				    row.createCell(commentCellWidthPerc, finding.getSRCOMNT());
	    				    row.getCells().forEach(cell -> {
						    	cell.setFontSize(10);
						    	cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
						    });
	    				});
	    			});
	    	});
	    
		return table;
	}
	
	private BaseTable generateGroupSexTable(PDDocument pdfDoc, PDPage pdfPage, List<SRDomainFinding> srDomainFindings,
			float yStart, float margin, ReportSpecimenOrdering reportSpecimenOrdering) throws IOException {
		// starting y position is whole page height subtracted by top and bottom margin
	    float yStartNewPage = pdfPage.getMediaBox().getHeight() - (2 * margin);
	    // we want table across whole page width (subtracted by left and right margin of course)
	    float tableWidth = pdfPage.getMediaBox().getWidth() - (2 * margin);
	    boolean drawContent = true;
	    float bottomMargin = 70;
	    
	    List<String> columnTitles = new ArrayList<>(Arrays.asList("Specimen/   Body System", "Test/Finding/ Category", "Original Result", "Domain", "Manifes-tation",
	    		"Severity", "% Inci-dence", "Qualifier", "Signifi-cance", "Treatment Related?", "Comment"));
	    
	    float cellWidthPerc = 100/(columnTitles.size());
	    float specimenCellWidthPerc = cellWidthPerc + 2.25f;
	    float domainCellWidthPerc = cellWidthPerc - 2.5f;
	    float manifestationCellWidthPerc = cellWidthPerc - 1.75f;
	    float commentCellWidthPerc = 100 - 3 * specimenCellWidthPerc - 3 * domainCellWidthPerc - 2 * manifestationCellWidthPerc - 2 * cellWidthPerc;
	    
	    BaseTable table = new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, margin, pdfDoc, pdfPage, true, drawContent);
		Row<PDPage> headerRow = table.createRow(15);
	    columnTitles.forEach(columnTitle -> {
			float cellWidth = cellWidthPerc;
			if (columnTitle.equals("Specimen/   Body System") || columnTitle.equals("Test/Finding/ Category") || columnTitle.equals("Original Result")) {
				cellWidth = specimenCellWidthPerc;
			} else if (columnTitle.equals("Domain") || columnTitle.equals("% Inci-dence") || columnTitle.equals("Signifi-cance")) {
				cellWidth = domainCellWidthPerc;
			} else if (columnTitle.equals("Manifes-tation") || columnTitle.equals("Severity")) {
				cellWidth = manifestationCellWidthPerc;
			} else if (columnTitle.equals("Comment")) {
				cellWidth = commentCellWidthPerc;
			}
			
			Cell<PDPage> cell = headerRow.createCell(cellWidth, columnTitle);
		    cell.setFont(PDType1Font.HELVETICA_BOLD);
		    cell.setFontSize(10);
		    cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
		});
	   
	    table.addHeaderRow(headerRow);
	    Map<String, Map<String, List<SRDomainFinding>>> findingGroups = new HashMap<>();
	    if (reportSpecimenOrdering == ReportSpecimenOrdering.group_domain_with_specimen) {
	    	findingGroups = srDomainFindings.stream()
			    	.collect(Collectors.groupingBy(SRDomainFinding::getSRSPEC,
			    			Collectors.groupingBy(SRDomainFinding::getSRDOMAIN)));
	    } else if (reportSpecimenOrdering == ReportSpecimenOrdering.specimen_with_group_domain) {
	    	findingGroups = srDomainFindings.stream()
			    	.collect(Collectors.groupingBy(SRDomainFinding::getSRDOMAIN,
			    			Collectors.groupingBy(SRDomainFinding::getSRSPEC)));
	    }
	    
	    findingGroups.values().stream()
	    	.forEach(findingGroup -> {
	    		findingGroup.values().stream()
	    			.forEach(findingsubGroup -> {
	    				findingsubGroup.forEach(finding -> {
	    					Row<PDPage> row = table.createRow(15);
	    				    row.createCell(specimenCellWidthPerc, finding.getSRSPEC());
	    				    String testOrFinding = (finding.getSRFNDG() != null && !finding.getSRFNDG().trim().equals(""))
	    				    		? finding.getSRFNDG()
	    				    		: finding.getSRTSTCD();
	    				    List<CodedValue> codedValues = cdiscService.findTestCdCodedValuesByDomain(SDomain.valueOf(finding.getSRDOMAIN()));
	    				    CodedValue studyTestCodedValue = codedValues != null
	    				    		? codedValues.stream().filter(studyTestCV -> studyTestCV.getName().equals(testOrFinding)).findAny().orElse(null)
	    				    		: null;
	    				    String testOrFindingName = studyTestCodedValue == null
	    				    		? testOrFinding
	    				    		: studyTestCodedValue.getPreferredTerm();
	    				    row.createCell(specimenCellWidthPerc, testOrFindingName);
	    				    row.createCell(specimenCellWidthPerc, finding.getSRORES());
	    				    row.createCell(domainCellWidthPerc, finding.getSRDOMAIN());
	    				    String manifestation = EnumUtils.isValidEnum(Manifestation.class, finding.getSROBSV())
						    		? Manifestation.valueOf(finding.getSROBSV()).getDisplayName()
						    		: "";
	    				    row.createCell(manifestationCellWidthPerc, manifestation);
	    				    row.createCell(manifestationCellWidthPerc, finding.getSRSEV());
	    				    row.createCell(domainCellWidthPerc, finding.getSRPCNT());
	    				    row.createCell(cellWidthPerc, finding.getSROBSQ());
	    				    row.createCell(domainCellWidthPerc, finding.getSRSIGF());
	    				    String treatmentRelated = EnumUtils.isValidEnum(TreatmentRelated.class, finding.getSRTRTEF())
						    		? TreatmentRelated.valueOf(finding.getSRTRTEF()).getDisplayName()
						    		: "";
	    				    row.createCell(cellWidthPerc, treatmentRelated);
	    				    row.createCell(commentCellWidthPerc, finding.getSRCOMNT());
	    				    row.getCells().forEach(cell -> {
						    	cell.setFontSize(10);
						    	cell.setBorderStyle(new LineStyle(Color.BLACK, (float) 0.25));
						    });
	    				});
	    			});
	    	});

		return table;
	}
	
	private float drawGroupSexTables(PDDocument pdfDoc, PDPage pdfPage, PDPageContentStream content, List<SRDomainFinding> srDomainFindings,
			float tableXStart, float yStart, float titleYOffset, float margin, PDFont tableTitleFont, int tableTitleFontSize,
			ReportSpecimenOrdering reportSpecimenOrdering, ReportEffectCause reportEffectCause, String reportDomain) throws IOException {
	    Map<String, Map<String, List<SRDomainFinding>>> doseGroups = srDomainFindings.stream()
	    		.filter(finding -> !finding.getSRDOMAIN().trim().equals(""))
	    		.filter(finding -> reportDomain == null || finding.getSRDOMAIN().equals(reportDomain))
	    		.filter(finding -> reportEffectCause == ReportEffectCause.ALL_EFFECTS ||
    				(finding.getSRTRTEF() != null && finding.getSRTRTEF().equals(reportEffectCause.getSrDomainName())))
		    	.collect(Collectors.groupingBy(SRDomainFinding::getSPGRPCD,
		    			Collectors.groupingBy(SRDomainFinding::getSRSEX)));	
	    
	    float tableTitleMargin = 10;
	    float tableYStart = yStart;
	    float tableEnd = 0;
	    float newLineOffset = 0;
	    
	    for (Map.Entry<String, Map<String, List<SRDomainFinding>>> doseGroup : doseGroups.entrySet()) {
	    	String doseGroupId = doseGroup.getKey();
	    	Map<String, List<SRDomainFinding>> sexGroups = doseGroup.getValue();
	    	for (Map.Entry<String, List<SRDomainFinding>> entry : sexGroups.entrySet()) {
	    		List<SRDomainFinding> findings = entry.getValue();
	    		SRDomainFinding sampleFinding = findings.get(0); // all findings in each group are supposed to have the same values for these fields
	    		String groupName = sampleFinding.getGRPLBL();
	    		String groupDose = sampleFinding.getSRGRPDOS();
	    		String sex = entry.getKey();
			    String sexName = EnumUtils.isValidEnum(Sex.class, sex)
			    		? Sex.valueOf(sex).getDisplayName()
			    		: "";
			    
			    if (sexName.equals("Both")) {
			    	sexName = "Male";
					String tableTitle = "Dose Group:" + doseGroupId + "; " + groupName + "; " + groupDose + "; Sex = " + sexName;
					BaseTable table = this.generateGroupSexTable(pdfDoc, pdfPage, findings, tableYStart, margin, reportSpecimenOrdering);
					table.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, table.getWidth(), 20, "left", 100, false);
					tableEnd = table.draw();
					
					if (table.getCurrentPage() != pdfPage) {					
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						content.endText();
						content.close();
						pdfPage = table.getCurrentPage();
						content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
						content.beginText();
						content.setFont(tableTitleFont, tableTitleFontSize);
						newLineOffset += tableTitleMargin;
						titleYOffset = tableEnd - margin + tableTitleMargin;
						tableXStart = margin;
					} else {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						titleYOffset = -Math.abs(tableEnd - tableYStart) - margin;
						tableXStart = 0;
					}
					
					tableYStart = tableEnd - margin;
					
					sexName = "Male";
					tableTitle = "Dose Group:" + doseGroupId + "; " + groupName + "; " + groupDose + "; Sex = " + sexName;
					table = this.generateGroupSexTable(pdfDoc, pdfPage, findings, tableYStart, margin, reportSpecimenOrdering);
					table.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, table.getWidth(), 20, "left", 100, false);
					tableEnd = table.draw();
					
					if (table.getCurrentPage() != pdfPage) {					
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						content.endText();
						content.close();
						pdfPage = table.getCurrentPage();
						content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
						content.beginText();
						content.setFont(tableTitleFont, tableTitleFontSize);
						newLineOffset += tableTitleMargin;
						titleYOffset = tableEnd - margin + tableTitleMargin;
						tableXStart = margin;
					} else {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						titleYOffset = -Math.abs(tableEnd - tableYStart) - margin;
						tableXStart = 0;
					}
					
					tableYStart = tableEnd - margin;
			    } else {
			    	String tableTitle = "Dose Group:" + doseGroupId + "; " + groupName + "; " + groupDose + "; Sex = " + sexName;
					BaseTable table = this.generateGroupSexTable(pdfDoc, pdfPage, findings, tableYStart, margin, reportSpecimenOrdering);
					table.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, table.getWidth(), 20, "left", 100, false);
					tableEnd = table.draw();
					
					if (table.getCurrentPage() != pdfPage) {					
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						content.endText();
						content.close();
						pdfPage = table.getCurrentPage();
						content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
						content.beginText();
						content.setFont(tableTitleFont, tableTitleFontSize);
						newLineOffset += tableTitleMargin;
						titleYOffset = tableEnd - margin + tableTitleMargin;
						tableXStart = margin;
					} else {
						newLineOffset = titleYOffset;
						content.newLineAtOffset(tableXStart, newLineOffset);
						titleYOffset = -Math.abs(tableEnd - tableYStart) - margin;
						tableXStart = 0;
					}
					
					tableYStart = tableEnd - margin;
			    }
	    	}
	    }
	    
	    content.endText();
		content.close();
	    
	    return tableYStart;
	}
	
	@Override
	public ByteArrayOutputStream generatePDFReport(Document document, ReportObservationOrdering observationOrder,
			ReportSpecimenOrdering reportSpecimenOrdering, ReportEffectCause reportEffectCause, String reportDomain) {
		try {
			PDDocument pdfDoc = new PDDocument();
			PDPage pdfPage = new PDPage(new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth()));
			ByteArrayOutputStream pdfByteArray = new ByteArrayOutputStream();
			List<SRDomainFinding> srDomainFindings = document.getSrDomainFindings();
			pdfDoc.addPage(pdfPage);
			PDPageContentStream content = new PDPageContentStream(pdfDoc, pdfPage);
			
			float startPositionX = 50;
			float startPositionY = 500;
			float tableMargin = 50;
			float tableTitleMargin = 10;
			float textLeading = 25;
			float docTitleFont = 18;
			int tableTitleFontSize = 15;
			PDType1Font tableTitleFont =  PDType1Font.HELVETICA_BOLD;
			PDType1Font titleFont = PDType1Font.HELVETICA_BOLD;
			String title = "Study report outcomes: " + document.getStudyId() + " - " + reportEffectCause.getDisplayName();
			String titleOrder= observationOrder == ReportObservationOrdering.by_domain
					? " (ordered by domain)"
					: " (ordered by group)";
			content.beginText();
			content.setLeading(textLeading);
			content.setFont(titleFont, docTitleFont);
			content.newLineAtOffset(startPositionX, startPositionY);
			boolean titleBreak = title.length() > 80;
			while (titleBreak) {
				content.showText(title.substring(0, 80));
				content.newLine();
				title = title.substring(80);
				titleBreak = title.length() > 80;
			}
			
			content.showText(title);
			content.showText(titleOrder);
			content.setFont(tableTitleFont, tableTitleFontSize);
			
			float docTitleMargin = textLeading + 15;
			content.newLineAtOffset(0, -docTitleMargin);
			float tableYStart = startPositionY - docTitleMargin - tableTitleMargin - textLeading;
			BaseTable doseGroupTable = this.generateDoseGroupTable(pdfDoc, pdfPage, srDomainFindings, tableYStart, tableMargin);
			String tableTitle = "Dose groups";
			doseGroupTable.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, doseGroupTable.getWidth(), 20, "left", 100, false);
			float tableEnd = doseGroupTable.draw();
			if (doseGroupTable.getCurrentPage() != pdfPage) {
				content.endText();
				content.close();
				pdfPage = doseGroupTable.getCurrentPage();
				content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
				content.beginText();
				content.setFont(tableTitleFont, tableTitleFontSize);
			}
			
			float nextTableTitleOffset = -Math.abs(tableEnd - tableYStart) - tableMargin;
			tableYStart = tableEnd - tableMargin;
			content.newLineAtOffset(0, nextTableTitleOffset);
			BaseTable domainTable = this.generateDomainTable(pdfDoc, pdfPage, srDomainFindings, tableYStart, tableMargin);
			tableTitle = "Domains";
			domainTable.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, domainTable.getWidth(), 20, "left", 100, false);
			tableEnd = domainTable.draw();
			if (domainTable.getCurrentPage() != pdfPage) {
				content.endText();
				content.close();
				pdfPage = domainTable.getCurrentPage();
				content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
				content.beginText();
				content.setFont(tableTitleFont, tableTitleFontSize);
			}
			
			tableYStart = tableEnd - tableMargin;
			
			/*
			nextTableTitleOffset = -Math.abs(tableEnd - tableYStart) - tableMargin;
			tableYStart = tableEnd - tableMargin;
			content.newLineAtOffset(0, nextTableTitleOffset);
			content.showText("Effect Level Results");
			BaseTable effectLevelTable = this.generateEffectLevelTable(pdfDoc, pdfPage, srDomainFindings, tableYStart, tableMargin);
			tableEnd = effectLevelTable.draw();
			if (effectLevelTable.getCurrentPage() != pdfPage) {
				content.endText();
				content.close();
				pdfPage = effectLevelTable.getCurrentPage();
				content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
				content.beginText();
				content.setFont(tableTitleFont, tableTitleFontSize);
			}
			*/
			
			float titleYOffset = tableYStart + tableTitleMargin;
			
			BaseTable effectLevelTable = this.generateEffectLevelTable(pdfDoc, pdfPage, srDomainFindings, tableYStart, tableMargin);
			tableTitle = "Effect Level Results";
			effectLevelTable.drawTitle(tableTitle, tableTitleFont, tableTitleFontSize, effectLevelTable.getWidth(), 20, "left", 100, false);
			tableEnd = effectLevelTable.draw();
			if (effectLevelTable.getCurrentPage() != pdfPage) {
				nextTableTitleOffset = titleYOffset;
				content.newLineAtOffset(startPositionX, nextTableTitleOffset);
				content.endText();
				content.close();
				pdfPage = effectLevelTable.getCurrentPage();
				content = new PDPageContentStream(pdfDoc, pdfPage, AppendMode.APPEND, true);
				content.beginText();
				content.setFont(tableTitleFont, tableTitleFontSize);
				nextTableTitleOffset += tableTitleMargin;
				titleYOffset = tableEnd - tableMargin + tableTitleMargin;
				startPositionX = tableMargin;
			} else {
				nextTableTitleOffset = titleYOffset;
				content.newLineAtOffset(startPositionX, nextTableTitleOffset);
				titleYOffset = -Math.abs(tableEnd - tableYStart) - tableMargin;
				startPositionX = 0;
			}
			
			
			tableYStart = tableEnd - tableMargin;
			if (observationOrder == ReportObservationOrdering.by_domain) {
				tableEnd = this.drawDomainSexTables(pdfDoc, pdfPage, content, srDomainFindings, startPositionX, tableYStart,
						titleYOffset, tableMargin, tableTitleFont, tableTitleFontSize, reportSpecimenOrdering, reportEffectCause, reportDomain);
			} else {
				tableEnd = this.drawGroupSexTables(pdfDoc, pdfPage, content, srDomainFindings, startPositionX, tableYStart,
						titleYOffset, tableMargin, tableTitleFont, tableTitleFontSize, reportSpecimenOrdering, reportEffectCause, reportDomain);
			}
			
			//content.endText();
			content.close();
			
			pdfDoc.save(pdfByteArray);
			pdfDoc.close();
			
			return pdfByteArray;
		} catch(IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
