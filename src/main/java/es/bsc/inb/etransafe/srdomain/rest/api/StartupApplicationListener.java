package es.bsc.inb.etransafe.srdomain.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import es.bsc.inb.etransafe.srdomain.rest.api.config.CDISCSENDLoader;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private CDISCSENDLoader loader;
	
    @Override 
    public void onApplicationEvent(ContextRefreshedEvent event) {
        loader.execute();
    }
}
