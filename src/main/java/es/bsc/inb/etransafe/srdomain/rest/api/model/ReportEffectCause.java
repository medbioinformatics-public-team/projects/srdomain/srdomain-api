package es.bsc.inb.etransafe.srdomain.rest.api.model;

import java.util.HashMap;
import java.util.Map;

public enum ReportEffectCause {

	ALL_EFFECTS ("A"), // Internal use. No finding will contain this value
	TREATMENT_RELATED("Y"),
	NOT_TREATMENT_RELATED("N"),
	UNCERTAIN("U");
	
	private String srDomainName;
	
	private static Map<ReportEffectCause, String> displayNames = new HashMap<>();
	static {
		displayNames.put(ALL_EFFECTS, "All effects");
		displayNames.put(NOT_TREATMENT_RELATED, "Non treatment related");
		displayNames.put(TREATMENT_RELATED, "Treatment related");
		displayNames.put(UNCERTAIN, "Uncertain");
	}
	
	ReportEffectCause(String srDomainName) {
		this.srDomainName = srDomainName;
	}
	
	public String getSrDomainName() {
		return srDomainName;
	}
	
	public String getDisplayName() {
		return displayNames.get(this);
	}
}
