package es.bsc.inb.etransafe.srdomain.rest.api.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.ControlledTerms;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.FieldName;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Finding;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.SRDomain;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Severity;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.Specimen;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.TestShortName;

public class Constants {

	public static int  SLICE_START = 1000;
	public static int  SLICE_END = 2000;
	public static int POINTS_INT = 5;
	public static String POINTS = ".....";
	
	
	public static Map<String, String> SEND_DOMAIN_NAMES = new HashMap<>(); 
	static {
		SEND_DOMAIN_NAMES.put("LB", "Laboratory Test Results");
		SEND_DOMAIN_NAMES.put("BW", "Body Weight");
		SEND_DOMAIN_NAMES.put("BG", "Body Weight Gain");
		SEND_DOMAIN_NAMES.put("CV", "Cardiovascular Domain");
		SEND_DOMAIN_NAMES.put("CL", "Clinical");
		SEND_DOMAIN_NAMES.put("DD", "Death Diagnosis");
		SEND_DOMAIN_NAMES.put("EG", "ECG Test Results");
		SEND_DOMAIN_NAMES.put("FM", "Fetal Measurements");
		SEND_DOMAIN_NAMES.put("FW", "Food And Water");
		SEND_DOMAIN_NAMES.put("FX", "Fetal Pathology Findings");
		SEND_DOMAIN_NAMES.put("MA", "Macroscopic Findings");
		SEND_DOMAIN_NAMES.put("MI", "Microscopic Findings");
		SEND_DOMAIN_NAMES.put("OM", "Organ Measurements");
		SEND_DOMAIN_NAMES.put("RE", "Respiratory Findings");
		SEND_DOMAIN_NAMES.put("VS", "Vital Signs");
	    
	};
	
	public static Map<FieldName, String> SR_FIELD_NAME_DESCRIPTIONS = new HashMap<>();
	static {
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.STUDYID, "Study identifier");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.DOMAIN, "Domain abbreviation");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRSEQ, "Sequence number");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRRISK, "Risk level associated with this group/sex");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SPGRPCD, "Sponsor-defined group code");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.GRPLBL, "Group name");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRGRPDOS, "Group dose level");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRSEX, "Sex");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRSTDY, "Study day of start of finding");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRSTPHSE, "Study phase of first observation");
		SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SROBSTDY, "Study phase day of observation");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRENDY, "Study day of end of finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRENPHSE, "Study phase of last observation");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SROBENDY, "End phase day of observation");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRDOMAIN, "Domain of finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRSPEC, "Specimen of finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRTSTCD, "Test short name");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRFNDG, "Finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRORES, "Observation (original result)");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SROBSV, "Manifestation of finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SROBSQ, "Observation of finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRSEV, "Severity of finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRPCNT, "Scale of finding");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRSIGF, "Statistical significance");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRTRTEF, "Treatment-related?");
	    SR_FIELD_NAME_DESCRIPTIONS.put(FieldName.SRCOMNT, "Comment");
	};
	
	public static Map<FieldName, List<ControlledTerms>> SR_FIELD_CODELISTS = new HashMap<>();
	static {
		SR_FIELD_CODELISTS.put(FieldName.STUDYID, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.DOMAIN, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRSEQ, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRRISK, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SPGRPCD, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.GRPLBL, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRGRPDOS, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRSEX, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRSTDY, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRSTPHSE, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SROBSTDY, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRENDY, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRENPHSE, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SROBENDY, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRDOMAIN, Arrays.asList(SRDomain.values()));
		SR_FIELD_CODELISTS.put(FieldName.SRSPEC, Arrays.asList(Specimen.values()));
		SR_FIELD_CODELISTS.put(FieldName.SRTSTCD, Arrays.asList(TestShortName.values()));
		SR_FIELD_CODELISTS.put(FieldName.SRFNDG, Arrays.asList(Finding.values()));
		SR_FIELD_CODELISTS.put(FieldName.SRORES, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SROBSV, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SROBSQ, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRSEV, Arrays.asList(Severity.values()));
		SR_FIELD_CODELISTS.put(FieldName.SRPCNT, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRSIGF, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRTRTEF, Arrays.asList());
		SR_FIELD_CODELISTS.put(FieldName.SRCOMNT, Arrays.asList());
	};
	
}
