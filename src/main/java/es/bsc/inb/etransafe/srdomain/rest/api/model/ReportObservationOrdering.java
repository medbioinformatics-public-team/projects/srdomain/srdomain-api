package es.bsc.inb.etransafe.srdomain.rest.api.model;

public enum ReportObservationOrdering {

	by_domain, by_group;
	
}
