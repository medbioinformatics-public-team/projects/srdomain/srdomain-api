package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum Finding implements ControlledTerms {
	NEOPLASM,
	NONNEO,
	CLCAT;
	
	@Override
	public String toName() {
		return this.name();
	}
}
