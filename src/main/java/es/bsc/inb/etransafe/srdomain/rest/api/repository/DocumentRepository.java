package es.bsc.inb.etransafe.srdomain.rest.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import es.bsc.inb.etransafe.srdomain.rest.api.model.Document;

@Repository
public interface DocumentRepository extends DocumentRepositoryCustom, MongoRepository<Document, String> {
	
	List<Document> findByStudyId(String name);
	
	Optional<Document> findById(String id);
		
	String findDocumentTextSubstringById(String id, Integer start, Integer end);

	List<Document> findAllStudyIds(); 
}
