package es.bsc.inb.etransafe.srdomain.rest.api.model;

import java.util.Date;

public class Tracking {

	private Date date;
	
	private String userName;
	
	private Action action;
	
	private String comment;

	
	public Tracking() {
		super();
	}

	public Tracking(String userName, Action action, String comment) {
		super();
		this.date=new Date();
		this.userName = userName;
		this.action = action;
		this.comment = comment;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
