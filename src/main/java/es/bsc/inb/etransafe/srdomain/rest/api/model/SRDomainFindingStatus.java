package es.bsc.inb.etransafe.srdomain.rest.api.model;

public enum SRDomainFindingStatus {
	non_curated, rejected, accepted, added;
}
