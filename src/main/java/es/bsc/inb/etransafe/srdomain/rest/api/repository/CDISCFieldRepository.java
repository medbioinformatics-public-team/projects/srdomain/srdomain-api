package es.bsc.inb.etransafe.srdomain.rest.api.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCField;

@Repository
public interface CDISCFieldRepository extends DocumentRepositoryCustom, MongoRepository<CDISCField, String> {
	
	CDISCField findBy_id(ObjectId _id);
	
	CDISCField findByName(String name);
	
}
