package es.bsc.inb.etransafe.srdomain.rest.api.model;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

@org.springframework.data.mongodb.core.mapping.Document(collection="CDISC_fields")
public class CDISCField {
	
	@Field("_id")
	@Id
	private ObjectId _id;
	
	private String name;
	
	private String description;
	
	private List<CodeList> codeLists;
	
	
	public CDISCField(String name, String description) {
		this.name = name;
		this.description = description;
		this.codeLists = new ArrayList<>();
	}
	
	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CodeList> getCodeLists() {
		return codeLists;
	}

	public void setCodeLists(List<CodeList> codeLists) {
		this.codeLists = codeLists;
	}
}
