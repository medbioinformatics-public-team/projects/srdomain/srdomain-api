package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum TestShortName implements ControlledTerms {
	BGTESTCD,
	BWTESTCD,
	DDTESTCD,
	EGTESTCD,
	FWTESTCD,
	LBTESTCD,
	OMTESTCD,
	PKPARMCD,
	SCVTSTCD,
	SRETSTCD,
	SVSTSTCD,
	
	NOTEST;
	
	@Override
	public String toName() {
		return this.name();
	}
}
