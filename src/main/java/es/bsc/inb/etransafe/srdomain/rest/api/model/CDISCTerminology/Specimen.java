package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum Specimen implements ControlledTerms {
	SPEC,
	BODSYS;
	
	@Override
	public String toName() {
		return this.name();
	}
}
