package es.bsc.inb.etransafe.srdomain.rest.api.model;

public enum ReportSpecimenOrdering {

	group_domain_with_specimen, specimen_with_group_domain;
	
}
