package es.bsc.inb.etransafe.srdomain.rest.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("es.bsc.inb.etransafe")
public class App {

	public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
	
}