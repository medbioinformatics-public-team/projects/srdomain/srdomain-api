package es.bsc.inb.etransafe.srdomain.rest.api.model;

import java.util.ArrayList;
import java.util.List;

public class CodeList {
	
	private String id;
	
	private String name;
	
	private String longName;
	
	private String description;
	
	private List<CodedValue> codedValues;
	
	
	
	public CodeList(String name) {
		this.name = name;
		this.codedValues = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CodedValue> getCodedValues() {
		return codedValues;
	}

	public void setCodedValues(List<CodedValue> codedValues) {
		this.codedValues = codedValues;
	}

	public String getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		this.longName = longName;
	}
}
