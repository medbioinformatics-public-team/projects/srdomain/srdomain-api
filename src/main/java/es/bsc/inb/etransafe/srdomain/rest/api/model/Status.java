package es.bsc.inb.etransafe.srdomain.rest.api.model;

public enum Status {
	 DRAFT, IN_PROGRESS, FINISHED, CLOSED  
}
