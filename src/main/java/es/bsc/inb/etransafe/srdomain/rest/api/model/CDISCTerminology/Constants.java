package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

import java.util.HashMap;
import java.util.Map;

public class Constants {
	
	public static Map<SDomain, TestShortName> SDOMAIN_TESTSHORTNAME = new HashMap<SDomain, TestShortName>();
	static {
	    
		
		SDOMAIN_TESTSHORTNAME.put(SDomain.BW, TestShortName.BWTESTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.BG, TestShortName.BGTESTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.CV, TestShortName.SCVTSTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.DD, TestShortName.DDTESTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.EG, TestShortName.EGTESTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.FW, TestShortName.FWTESTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.LB, TestShortName.LBTESTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.OM, TestShortName.OMTESTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.PP, TestShortName.PKPARMCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.RE, TestShortName.SRETSTCD);
		SDOMAIN_TESTSHORTNAME.put(SDomain.VS, TestShortName.SVSTSTCD);
	    
	    //put("FM", TestShortName.FWTESTCD); fetal measurements is no more part of the srdomain definition
	    //put("TF", TestShortName.TFTESTCD); tumor findings is no more part of the srdomain definition
	};
	
	
}
