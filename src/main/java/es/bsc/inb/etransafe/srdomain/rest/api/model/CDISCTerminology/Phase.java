package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum Phase implements ControlledTerms {
	PRE_DOSING,
	DOSING,
	RECOVERY;

	@Override
	public String toName() {
		return this.name();
	}
}
