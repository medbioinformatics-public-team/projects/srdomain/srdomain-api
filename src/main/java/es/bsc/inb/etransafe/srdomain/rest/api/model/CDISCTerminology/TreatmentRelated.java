package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum TreatmentRelated {
	Y ("Yes"),
	N ("No"),
	U ("Uncertain");
	
	private String displayName;
	
	TreatmentRelated(String displayName) {
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
}
