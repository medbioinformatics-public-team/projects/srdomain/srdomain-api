package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum Severity implements ControlledTerms {
	
	SEV;
	
	@Override
	public String toName() {
		return this.name();
	}
}
