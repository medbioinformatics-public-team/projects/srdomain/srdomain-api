package es.bsc.inb.etransafe.srdomain.rest.api.model;

public class CodedValue {
	
	private String id;
	
	private String name;
	
	private String preferredTerm;
	
	private String description;

	
	public CodedValue(String id, String name, String preferredTerm, String description) {
		this.id = id;
		this.name = name;
		this.preferredTerm = preferredTerm;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPreferredTerm() {
		return preferredTerm;
	}

	public void setPreferredTerm(String preferredTerm) {
		this.preferredTerm = preferredTerm;
	}
}
