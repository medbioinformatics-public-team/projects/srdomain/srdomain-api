package es.bsc.inb.etransafe.srdomain.rest.api.model;

public enum Action {
	 MOVE, REJECT, ACCEPT, EXPORT_SR_FINDING, NOT_EXPORT_SR_FINDING
}