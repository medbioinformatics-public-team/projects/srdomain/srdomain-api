package es.bsc.inb.etransafe.srdomain.rest.api.controllers;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import es.bsc.inb.etransafe.srdomain.rest.api.model.Document;
import es.bsc.inb.etransafe.srdomain.rest.api.model.DocumentTracking;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportEffectCause;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportObservationOrdering;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportSpecimenOrdering;
import es.bsc.inb.etransafe.srdomain.rest.api.model.Status;
import es.bsc.inb.etransafe.srdomain.rest.api.services.DocumentService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/reports")
public class DocumentController {

	@Autowired
	public DocumentService documentService;

	@GetMapping
	public List<Document> findAll() {
		return documentService.findAll();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Document> find(@PathVariable(value = "id") String id) {
		return new ResponseEntity<Document>(documentService.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = "/studyIds")
	public List<Document> findAllStudyIds() {
		return documentService.findAll();
	}

	@PostMapping
	public ResponseEntity<Document> create(@RequestBody Document document) {
		if (documentService.validateCreate(document)) {
			return new ResponseEntity<Document>(documentService.save(document), HttpStatus.CREATED);
		} else {
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
			String date = dateFormat.format(new Date());
			document.setStudyId(document.getStudyId() + "_" + date);
			//document.setSrDomainFindings(null);
			return new ResponseEntity<Document>(document, HttpStatus.CONFLICT);
		}
	}

	@PutMapping
	public ResponseEntity<Document> update(@RequestBody Document document) {
		return new ResponseEntity<Document>(documentService.save(document), HttpStatus.OK);
	}

	@GetMapping("/{id}/finding/{findingId}")
	// @RequestMapping("/{id}/finding/{findingId}")
	public String findFinding(@PathVariable(value = "id") Long id,
			@PathVariable(value = "findingId") Integer findingId) {
//		String snippet = documentService.findFindingEvidenceByDocumentIdAndFindingId(id, findingId);
//		return snippet;
		return null;
	}

	@PostMapping("{id}/move/{status}")
	public ResponseEntity<Void> moveDocument(@PathVariable(value = "id") String id,
			@PathVariable(value = "status") Status status) {
		documentService.moveDocument(id, status);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@GetMapping(value = "/export/{id}")
	public ResponseEntity<byte[]> generateSRDomainReport(@PathVariable(value = "id") String id) {
		Map<String, Object> file_data = documentService.exportToSRDomainFile(id);
		String filename = file_data.get("file_name") + ".csv";
		HttpHeaders head = new HttpHeaders();
		head.setContentType(MediaType.parseMediaType("text/csv"));
		head.add("content-disposition", "attachment; filename=" + filename);
		head.setContentDispositionFormData(filename, filename);
		head.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		return new ResponseEntity<>((byte[]) file_data.get("content"), head, HttpStatus.OK);
	}

	@PostMapping("/import")
	public ResponseEntity<List<String>> importSRDomain(@RequestParam("files") MultipartFile[] files) {
		try {
			List<String> fileNames = new ArrayList<>();
			List<String> errors = documentService.importSRDomainValidation(files);
			List<String> messages = new ArrayList<String>();
			if (errors.size() == 0) {
				messages.add("The operation was processed. Please review the associated comments for each document:");
				Arrays.asList(files).stream().forEach(file -> {
					Document document = documentService.importSRDomain(file);
					if (document == null) {
						messages.add("An error ocurred processing the file: " + file.getOriginalFilename()
								+ ". Please review that the file contains a valid SRDomain format. ");
					} else {
						messages.add("The file: " + file.getOriginalFilename() + " was imported correctly.");
					}
					fileNames.add(file.getOriginalFilename());
				});
			} else {
				errors.add("Validation fail, no document was uploaded. Please fix the errors and try again.");
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(errors);
			}
			return ResponseEntity.status(HttpStatus.OK).body(messages);
		} catch (Exception e) {
			e.printStackTrace();
			List<String> messages = new ArrayList<String>();
			messages.add("An error occurred importing the SRDomain files.");
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(messages);
		}
	}

	@GetMapping(value = "/pdfreport/{id}/{reportObservationOrdering}/{reportSpecimenOrdering}/{reportEffectCause}/{reportDomain}")
	public ResponseEntity<ByteArrayResource> generatePDFReport(@PathVariable(value = "id") String id,
			@PathVariable(value = "reportObservationOrdering") ReportObservationOrdering reportObservationOrdering,
			@PathVariable(value = "reportSpecimenOrdering") ReportSpecimenOrdering reportSpecimenOrdering,
			@PathVariable(value = "reportEffectCause") ReportEffectCause reportEffectCause,
			@PathVariable(value = "reportDomain") String reportDomain) {
		Document document = documentService.findById(id);
		if (document == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		String fileName = "Study_Report_" + document.getStudyId() + ".pdf";
		ByteArrayOutputStream pdfStream = documentService.generatePDFReport(document, reportObservationOrdering,
				reportSpecimenOrdering, reportEffectCause, reportDomain);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
				.header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
				.header(HttpHeaders.CACHE_CONTROL, "must-revalidate, post-check=0, pre-check=0")
				.body(new ByteArrayResource(pdfStream.toByteArray()));
	}
	
	@GetMapping(value = "/pdfreport/{id}/{reportObservationOrdering}/{reportSpecimenOrdering}/{reportEffectCause}")
	public ResponseEntity<ByteArrayResource> generatePDFReport(@PathVariable(value = "id") String id,
			@PathVariable(value = "reportObservationOrdering") ReportObservationOrdering reportObservationOrdering,
			@PathVariable(value = "reportSpecimenOrdering") ReportSpecimenOrdering reportSpecimenOrdering,
			@PathVariable(value = "reportEffectCause") ReportEffectCause reportEffectCause) {
		Document document = documentService.findById(id);
		if (document == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		String fileName = "Study_Report_" + document.getStudyId() + ".pdf";
		ByteArrayOutputStream pdfStream = documentService.generatePDFReport(document, reportObservationOrdering,
				reportSpecimenOrdering, reportEffectCause, null);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
				.header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
				.header(HttpHeaders.CACHE_CONTROL, "must-revalidate, post-check=0, pre-check=0")
				.body(new ByteArrayResource(pdfStream.toByteArray()));
	}

	@DeleteMapping(value = "/remove/{id}")
	public ResponseEntity<Void> removeDocument(@PathVariable(value = "id") String id) {
		documentService.removeDocument(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@PostMapping(value = "/restore/{id}")
	public ResponseEntity<Void> restoreDocument(@PathVariable(value = "id") String id) {
		documentService.restoreDocument(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@GetMapping("/tracking/{id}")
	public ResponseEntity<DocumentTracking> findTrackings(@PathVariable(value = "id") String id) {
		return new ResponseEntity<DocumentTracking>(documentService.findTrackings(id), HttpStatus.OK);
	}

}