package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum Risk {
	NOEL ("No Observed Effect Level"),
	LOEL ("Lowest Observed Effect Level"),
	NOAEL ("No Observed Adverse Effect Level"),
	LOAEL ("Lowest Observed Adverse Effect Level"),
	HNSTD ("Highest Non-Severely Toxic Dose"),
	STD ("Severely Toxic Dose"),
	MTD ("Maximum Tolerated Dose");
	
	private String displayName;
	
	Risk(String displayName) {
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
}
