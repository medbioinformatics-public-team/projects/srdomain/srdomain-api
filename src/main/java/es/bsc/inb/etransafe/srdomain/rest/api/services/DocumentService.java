package es.bsc.inb.etransafe.srdomain.rest.api.services;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;


import org.springframework.web.multipart.MultipartFile;
import es.bsc.inb.etransafe.srdomain.rest.api.model.Document;
import es.bsc.inb.etransafe.srdomain.rest.api.model.DocumentTracking;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportEffectCause;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportObservationOrdering;
import es.bsc.inb.etransafe.srdomain.rest.api.model.ReportSpecimenOrdering;
import es.bsc.inb.etransafe.srdomain.rest.api.model.Status;


public interface DocumentService  {

	List<Document> findAll();

	Document findById(String id);

	Document save(Document document);
	
	List<Document> findByDocumentsName(String name);
	
	Status moveDocument(String id, Status status);
	
	DocumentTracking findTrackings(String documentId);

	void removeDocument(String documentId); 
	
	void restoreDocument(String documentId);

	ByteArrayOutputStream generatePDFReport(Document document,ReportObservationOrdering observationOrder,
			ReportSpecimenOrdering reportSpecimenOrdering, ReportEffectCause reportEffectCause, String reportDomain);
	
	public Map<String, Object> exportToSRDomainFile(String reportId);

	List<String> importSRDomainValidation(MultipartFile[] files);

	Document importSRDomain(MultipartFile file);

	Boolean existDocumentByStudyId(String studyId);

	Boolean validateCreate(Document document);
}
