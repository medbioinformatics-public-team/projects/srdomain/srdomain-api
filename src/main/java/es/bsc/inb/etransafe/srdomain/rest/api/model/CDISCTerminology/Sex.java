package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum Sex {
	F ("Female"),
	M ("Male"),
	B ("Both"),
	U ("Unknown");
	
	private String displayName;
	
	Sex(String displayName) {
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
}
