package es.bsc.inb.etransafe.srdomain.rest.api.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class LivenessController {

  @GetMapping(path = "/liveness")
  public ResponseEntity<Void> liveness() {
      return new ResponseEntity<Void>(HttpStatus.OK);
  }
  
  @GetMapping(path = "/readiness")
  public ResponseEntity<Void> readeness() {
	  return new ResponseEntity<Void>(HttpStatus.OK);
  }
  
}
