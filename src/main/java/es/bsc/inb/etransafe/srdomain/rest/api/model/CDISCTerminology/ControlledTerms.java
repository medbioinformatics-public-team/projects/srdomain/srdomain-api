package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public interface ControlledTerms {
	
	public String toName();
	
}
