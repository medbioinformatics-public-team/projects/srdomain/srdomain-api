package es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology;

public enum SRDomain implements ControlledTerms {
	SDOMAIN;
	
	@Override
	public String toName() {
		return this.name();
	}
}
