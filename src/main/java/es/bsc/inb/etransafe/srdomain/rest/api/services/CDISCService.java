package es.bsc.inb.etransafe.srdomain.rest.api.services;

import java.util.List;
import java.util.Map;

import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCField;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodeList;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CodedValue;
import es.bsc.inb.etransafe.srdomain.rest.api.model.CDISCTerminology.SDomain;


public interface CDISCService {
	List<CDISCField> findAll();
	
	CDISCField findByFieldName(String fieldName);

	CDISCField save(CDISCField document);
	
	CodeList findTestCDByDomain(SDomain domain);
	
	Map<SDomain, CodeList> findAllTestCdsWithDomain();
	
	CDISCField findAllStudyDomains();
	
	List<CodedValue> findTestCdCodedValuesByDomain(SDomain sdomain);
	
	List<CodedValue> findFindingsByDomain(SDomain domain);
	
	List<CodedValue> findSpeciesByDomain(SDomain domain);
	
	List<CodedValue> findAllStudyDomainsCodedValues();

	List<CodedValue> findSeverityCodedValues();
}
