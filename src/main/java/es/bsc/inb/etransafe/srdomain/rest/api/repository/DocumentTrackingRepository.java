package es.bsc.inb.etransafe.srdomain.rest.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import es.bsc.inb.etransafe.srdomain.rest.api.model.DocumentTracking;

@Repository
public interface DocumentTrackingRepository extends DocumentTrackingRepositoryCustom, MongoRepository<DocumentTracking, String> {
	
	DocumentTracking findByDocumentId(String documentId);

}
