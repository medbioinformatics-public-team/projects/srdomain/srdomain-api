#!/bin/sh

BASEDIR=/usr/local
REST_API_HOME="${BASEDIR}/share/srdomain_api/"

REST_API_VERSION=1.0

# Exit on error
set -e

if [ $# -ge 1 ] ; then
	REST_API_VERSION="$1"
fi

if [ -f /etc/alpine-release ] ; then
	# Installing OpenJDK 8
	apk add --update openjdk8-jre
	
	# dict tagger development dependencies
	apk add openjdk8 git maven
else
	# Runtime dependencies
	apt-get update
	apt-get install openjdk-8-jre
	
	# The development dependencies
	apt-get install openjdk-8-jdk git maven
fi

mvn clean install -DskipTests

#rename jar
mv target/srdomain-api-0.0.1-SNAPSHOT.jar srdomain-api-${REST_API_VERSION}.jar

cat > /usr/local/bin/srdomain-api <<EOF
#!/bin/sh
exec java \$JAVA_OPTS -jar "${REST_API_HOME}/srdomain-api-${REST_API_VERSION}.jar" -workdir "${REST_API_HOME}" "\$@"
EOF
chmod +x /usr/local/bin/srdomain-api

#delete target
rm -R target src pom.xml

#add bash for nextflow
apk add bash

if [ -f /etc/alpine-release ] ; then
	# Removing not needed tools
	apk del openjdk8 git maven
	rm -rf /var/cache/apk/*
else
	apt-get remove openjdk-8-jdk git maven
	rm -rf /var/cache/dpkg
fi

