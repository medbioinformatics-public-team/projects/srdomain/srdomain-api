FROM alpine:3.9
WORKDIR /usr/local/share/srdomain_api

ARG	REST_API_VERSION=1.0
COPY	docker-build.sh /usr/local/bin/docker-build.sh
COPY	src src
COPY	pom.xml .

RUN mkdir logs
RUN chmod u=rwx,g=rwx,o=r /usr/local/share/srdomain_api -R

RUN	docker-build.sh ${REST_API_VERSION}

CMD  /usr/local/bin/srdomain-api 
 