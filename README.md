# SRDomain API

SRDomain API is one of the key components of the PretoxTM (Preclinical Toxicology Text Mining) project and SRDomain Web Editor.

SRDomain API is the REST API responsible for accessing the SR-Domain format controlled terminology. It is also the main REST API of the WEB SR-Domain editor solution and has access to the SRDomain DB. 

To know more about PretoxTM please refer to the PretoxTM Central Documentation: 
**[https://pretoxtm.gitlab.io/documentation/](https://pretoxtm.gitlab.io/documentation/)**.

## Installation

Go to the main **[PretoxTM gitlab page](https://gitlab.bsc.es/inb/etransafe/pretoxtm)**.

## Authors

* **Javier Corvi**
* **Nicolas Díaz Roussel**


## Citation

Pognan F, Steger-Hartmann T, Díaz C, Blomberg N, Bringezu F, Briggs K, Callegaro G, Capella-Gutierrez S, Centeno E, Corvi J, Drew P, Drewe WC, Fernández JM, Furlong LI, Guney E, Kors JA, Mayer MA, Pastor M, Piñero J, Ramírez-Anguita JM, Ronzano F, Rowell P, Saüch-Pitarch J, Valencia A, van de Water B, van der Lei J, van Mulligen E, Sanz F. The eTRANSAFE Project on Translational Safety Assessment through Integrative Knowledge Management: Achievements and Perspectives. Pharmaceuticals. 2021; 14(3):237. https://doi.org/10.3390/ph14030237 

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE Version 3 - see the [LICENSE](LICENSE) file for details





	
		
